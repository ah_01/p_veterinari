<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en">
<!--<![endif]-->
<!-- BEGIN HEAD -->
<head>
<meta charset="utf-8"/>
<title>Metronic | Blank Page Layout</title>
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta content="width=device-width, initial-scale=1.0" name="viewport"/>
<meta http-equiv="Content-type" content="text/html; charset=utf-8">
<meta content="" name="description"/>
<meta content="" name="author"/>
<!-- BEGIN GLOBAL MANDATORY STYLES -->
<link href="http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all" rel="stylesheet" type="text/css">
<link href="/metronic/assets/global/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
<link href="/metronic/assets/global/plugins/simple-line-icons/simple-line-icons.min.css" rel="stylesheet" type="text/css">
<link href="/metronic/assets/global/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css">
<link href="/metronic/assets/global/plugins/uniform/css/uniform.default.css" rel="stylesheet" type="text/css">
<!-- END GLOBAL MANDATORY STYLES -->

<!-- BEGIN PAGE LEVEL STYLES -->
	@yield('page_level')
<!-- END PAGE LEVEL STYLES -->

<!-- BEGIN THEME STYLES -->
<link href="/metronic/assets/global/css/components-md.css" id="style_components" rel="stylesheet" type="text/css">
<link href="/metronic/assets/global/css/plugins-md.css" rel="stylesheet" type="text/css">
<link href="/metronic/assets/admin/layout3/css/layout.css" rel="stylesheet" type="text/css">
<link href="/metronic/assets/admin/layout3/css/themes/default.css" rel="stylesheet" type="text/css" id="style_color">
<link href="/metronic/assets/admin/layout3/css/custom.css" rel="stylesheet" type="text/css">
<!-- END THEME STYLES -->
<link rel="shortcut icon" href="favicon.ico"/>
</head>

<body class="page-md">
		{{-- AHA ********************************************************************************************** --}}

@include('metronic.header')

{{-- @include('metronic.pagecontainer') --}}

<div class="page-container">
	<!-- BEGIN PAGE HEAD -->

	@include('flash::message')

	<div class="page-head">
		<div class="container">


		@yield('page_head')
		
		</div>
	</div>


	<div class="page-content">
		<div class="container">
	
			@yield('page_container')
		</div>
	</div>

</div>

	@yield('content')	




@include('metronic.parts.prefooter')

@include('metronic.footer')

		{{-- AHA ********************************************************************************************** --}}
<!-- BEGIN JAVASCRIPTS(Load javascripts at bottom, this will reduce page load time) -->
<!-- BEGIN CORE PLUGINS -->
<!--[if lt IE 9]>
<script src="/metronic/assets/global/plugins/respond.min.js"></script>
<script src="/metronic/assets/global/plugins/excanvas.min.js"></script> 
<![endif]-->
<script src="/metronic/assets/global/plugins/jquery.min.js" type="text/javascript"></script>
<script src="/metronic/assets/global/plugins/jquery-migrate.min.js" type="text/javascript"></script>
<!-- IMPORTANT! Load jquery-ui.min.js before bootstrap.min.js to fix bootstrap tooltip conflict with jquery ui tooltip -->
<script src="/metronic/assets/global/plugins/jquery-ui/jquery-ui.min.js" type="text/javascript"></script>
<script src="/metronic/assets/global/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
<script src="/metronic/assets/global/plugins/bootstrap-hover-dropdown/bootstrap-hover-dropdown.min.js" type="text/javascript"></script>
<script src="/metronic/assets/global/plugins/jquery-slimscroll/jquery.slimscroll.min.js" type="text/javascript"></script>
<script src="/metronic/assets/global/plugins/jquery.blockui.min.js" type="text/javascript"></script>
<script src="/metronic/assets/global/plugins/jquery.cokie.min.js" type="text/javascript"></script>
<script src="/metronic/assets/global/plugins/uniform/jquery.uniform.min.js" type="text/javascript"></script>
<!-- END CORE PLUGINS -->
<!-- BEGIN PAGE LEVEL PLUGINS -->
	@yield('page_plugins')  {{-- Yield additional java script --}}
<!-- END PAGE LEVEL PLUGINS -->


<script src="/metronic/assets/global/scripts/metronic.js" type="text/javascript"></script>
<script src="/metronic/assets/admin/layout3/scripts/layout.js" type="text/javascript"></script>
<script src="/metronic/assets/admin/layout3/scripts/demo.js" type="text/javascript"></script>
<script src="/metronic/assets/global/scripts/datatable.js"></script>
{{-- <script src="/metronic/assets/admin/pages/scripts/table-managed.js"></script> --}}
<script src="/metronic/assets/admin/pages/scripts/table-advanced.js"></script>
<script>
      jQuery(document).ready(function() {    
     	Metronic.init(); // init metronic core components
		Layout.init(); // init current layout
		Demo.init(); // init demo features
		// TableManaged.init();
		   TableAdvanced.init();

      });

// CSRF protection
// $.ajaxSetup(
// {
//     headers:
//     {
//         'X-CSRF-Token': $('input[name="_token"]').val()
//     }
// });
   </script>


  
<!-- END JAVASCRIPTS -->

</body>
<!-- END BODY -->
</html>