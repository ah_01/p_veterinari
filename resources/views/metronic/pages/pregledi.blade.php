@extends ('metronic')
{{-- @include ('metronic.parts.np_modal') --}}



@section('page_level')
	<link rel="stylesheet" type="text/css" href="{{ url('metronic') }}/assets/global/plugins/select2/select2.css"/>
	<link rel="stylesheet" type="text/css" href="{{ url('metronic') }}/assets/global/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.css"/>
	<link href="{{ url('metronic') }}/assets/global/plugins/bootstrap-modal/css/bootstrap-modal-bs3patch.css" rel="stylesheet" type="text/css"/>
	<link href="{{ url('metronic') }}/assets/global/plugins/bootstrap-modal/css/bootstrap-modal.css" rel="stylesheet" type="text/css"/>
	<link href="{{ url('metronic') }}/assets/global/css/components-rounded.css" rel="stylesheet" type="text/css"/>
	<link rel="stylesheet" type="text/css" href="{{ url('metronic') }}/assets/datepicker/css/datepicker.css"/>

@stop

@section('page_plugins')
	<script type="text/javascript" src="{{ url('metronic') }}/assets/global/plugins/select2/select2.min.js"></script>
	<script type="text/javascript" src="{{ url('metronic') }}/assets/global/plugins/datatables/media/js/jquery.dataTables.min.js"></script>
	<script type="text/javascript" src="{{ url('metronic') }}/assets/global/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.js"></script>
	<script src="{{ url('metronic') }}/assets/admin/pages/scripts/table-managed.js"></script>

	<script src="{{ url('metronic') }}/assets/global/plugins/bootstrap-modal/js/bootstrap-modalmanager.js" type="text/javascript"></script>
	<script src="{{ url('metronic') }}/assets/global/plugins/bootstrap-modal/js/bootstrap-modal.js" type="text/javascript"></script>
	<script src="{{ url('metronic') }}/assets/datepicker/js/bootstrap-datepicker.js"></script>


		<script>
				jQuery(document).ready(function() {
				//    Metronic.init(); // init metronic core components
				// Layout.init(); // init current layout
				// Demo.init(); // init demo features
				   TableManaged.init();
				   $(".select2").select2();

				   $('#datum_pregled').datepicker({
				   		format: 'yyyy-mm-dd'
				   });

				   $('#datum_nalaza').datepicker({
				   		format: 'yyyy-mm-dd'
				   });
				   // $("#zivotinja").select2(

				   // 	);

				   $('#kanton').change(function(){
				   			$.get("{{ url('api/kanton')}}",
				   				{ option: $(this).val() },
				   				function(data) {
				   					var imanje = $('#imanje');
				   					// console.log(data);
				   					$("#imanje").select2(

				   						);
				   					$('#imanje').empty().append("<option value='---'>---</option>");
				   					$('#zivotinja').empty().append("<option value='---'>---</option>");
				    				// return data;
				   			            // $('#imanje').append("<option value='---'>---</option>");
				   					$.each(data, function(index, element) {
				   						// console.log(element.id_broj);
				   			            $('#imanje').append("<option value='"+ element.id +"'>" + element.id_broj + "</option>");
				   			            // console.log(index + '|||||||' + element);
				   			        });
				   				});
				   		});
				   $('#imanje').change(function(){
				   			$.get("{{ url('api/zivotinje')}}",
				   				{ option: $(this).val() },
				   				function(data) {
				   					var zivotinja = $('#zivotinja');
				   					// console.log(data);
				   					// $("#zivotinja").select2(

				   					// 	);trazi
				   					// zivotinja.empty();
				    				// return data;
				   					$.each(data, function(index, element) {
				   						console.log(element);
				   			            $('#zivotinja').append("<option value='"+ index +"'>" + element + "</option>");
				   			            // console.log(index + '|||||||' + element);
				   			        });
				   				});
				   		});
				   $('#imanje').change(function(){
				   			$.get("{{ url('api/imanje')}}",
				   				{ option: $(this).val() },
				   				function(data) {
				   					// var zivotinja = $('#zivotinja');
				   					// console.log(data[0].ime);
				   					$("#imanje_ime1").val(data[0].ime);
				   					$("#imanje_adresa").val(data[0].adresa);
				   					$("#imanje_mjesto").val(data[0].mjesto);
				   					$("#imanje_pbroj").val(data[0].pbroj);

				   				});
				   		});


				});

		</script>

@stop

@section('content')


<div class="page-content-wrapper">
		<div class="page-content">
			<!-- BEGIN PAGE HEAD -->
			<div class="page-head">

{{-- 	BEGIN PAGE TITLE
	<div class="page-title">
		<h1>Dashboard <small>statistics & reports</small></h1>
	</div>
	END PAGE TITLE

</div> --}}
			<!-- END PAGE HEAD -->
			<!-- BEGIN PAGE BREADCRUMB -->

			<!-- END PAGE BREADCRUMB -->
			<!-- BEGIN PAGE CONTENT INNER -->

			<div class="row">
				<div class="col-md-12">


				@if (count($errors) > 0)
				    <div class="alert alert-danger">
				        <ul>
				            @foreach ($errors->all() as $error)
				                <li>{{ $error }}</li>
				            @endforeach
				        </ul>
				    </div>
				@endif



					<div class="portlet box green tabbable">
											<div class="portlet-title">
												<div class="col-lg-9 caption">
													<i class="fa fa-gift"></i>Pregledi <br>
													<?php $columnSizes = [
													  'sm' => [4, 8],
													  'lg' => [2, 10]
													]; ?>

												</div>



												<ul class="nav nav-tabs">

													<li>
{{-- 														<a href="#portlet_tab3" data-toggle="tab">
														Tab 3 </a> --}}
													</li>
											{{-- 		<li>
														<a href="#portlet_tab2" data-toggle="tab">
														Tab 2 </a>
													</li>
													<li class="active">
														<a href="#portlet_tab1" data-toggle="tab">
														Tab 1 </a>
													</li> --}}
												</ul>

											</div>


											<div class="portlet-body">
													<!-- <a class="btn blue btn-xs" data-toggle="modal" href="#responsive" style="margin:1px auto;"> Novi pregled </a> -->
													<a class="btn blue btn-xs" data-toggle="modal" href="/pregledi/p/stanica" style="margin:1px auto;"> Pregledi stanice </a>
													<a class="btn blue btn-xs" data-toggle="modal" href="/pregledi/p/korisnik" style="margin:1px auto;"> Moji pregledi </a>

													<hr>

													{{-- ------------------------------------------- START: Trazi imanja ---------------------------------------------------  --}}
													 <div class="alert alert-info">

													     <div class="col-lg-3">

													       {!! BootForm::open()->action('/pregledi/pretraga')->method('POST') !!}


													            {!! BootForm::text('godina', 'godina')->class('form-control input-sm') !!}

													     </div>
													     <div class="col-lg-3">
													            {!! BootForm::select('imanje_id_broj', 'imanje_id_broj')->options($imanja)->class("form-control select2me input-sm"); !!}

													     </div>

													     <div class="col-lg-3">
													            {!! BootForm::text('imanje_ime', 'imanje_ime')->class('form-control input-sm') !!}

													     </div>


													     <div class="col-lg-3 text-center">
													           {!! BootForm::submit('Trazi')->class('btn blue btn-sm') !!}
													            {!! BootForm::close() !!}
													     </div>
													   <div class=""> <br><br><br> </div>
													   </div>
													{{-- ------------------------------------------- START: Trazi imanja ---------------------------------------------------  --}}


													{{-- ------------------------------------------- START: PRINT report ---------------------------------------------------  --}}
														@if (isset($imanje))
													<div class="alert alert-info">
													<h3>Print izvjestaja</h3>
													<p>
													 Pritiskom na sljedece dugme generisati cete PDF izvjestaj vase pretrage.
													 {!! BootForm::open()->action("/pregledi/printp")->post()->class('form-inline')->target("_blank") !!}
																	{!! BootForm::hidden('godina')->value($godina) !!}
																	{!! BootForm::hidden('imanje')->value($imanje) !!}

																	{!! BootForm::submit('PRINT')->class('btn red btn-xs') !!}
																{!! BootForm::close() !!}

													</p>
													<h3>Print izvjestaja</h3>
													<p>
													 Pritiskom na sljedece dugme generisati cete PDF izvjestaj vase pretrage.
													 {!! BootForm::open()->action("/pregledi/print")->post()->class('form-inline')->target("_blank") !!}
																	{!! BootForm::hidden('godina')->value($godina) !!}
																	{!! BootForm::hidden('imanje')->value($imanje) !!}

																	{!! BootForm::submit('PRINT-HTML')->class('btn red btn-xs') !!}
																{!! BootForm::close() !!}

													</p>
													<div class="col-lg-2 row">
	{{-- 														<div class="col-lg-2">
																{!! BootForm::open()->action("/pregledi/print")->post()->class('form-inline')->target("_blank") !!}
																	{!! BootForm::hidden('godina')->value($godina) !!}
																	{!! BootForm::hidden('imanje')->value($imanje) !!}

																	{!! BootForm::submit('P')->class('btn red btn-xs') !!}
																{!! BootForm::close() !!}

															</div> --}}

													</div>
													</div>
														@endif



													{{-- ------------------------------------------- START: PRINT report ---------------------------------------------------  --}}



											<hr>
												<div class="tab-content">
													<div class="tab-pane active" id="portlet_tab1">


														<table class="table table-striped table-bordered table-hover" id="sample_4" style="width:99% !important; font-size: 11">
																					<thead>
																					<tr>
																						<th class="table-checkbox">
																							id
																						</th>
																						<th>imanje_id_broj</th>
																						<th><span style="font-size: 5pt;">korisnik_id | stanica_id</span> Naziv </th>
																						<th>broj_zivotinje</th>
																						<th>datum_pregled</th>
																						<th>pregled</th>
																						<th>rezultat_pregled</th>
																						<th>imanje_ime</th>
{{-- 																						<th>imanje_mjesto</th>
																						<th>imanje_adresa</th> --}}

																						<th>Akcije</th>

																					</tr>
																					</thead>
																					<tbody>
																							@foreach ($data as $q)



																								<tr>
																									<td>{!! link_to("pregledi/$q->id", $q->id) !!}</td>
																									<td>{!! $q->imanje_id_broj !!}</td>
																									<td>
																									{{$q->naziv_pregled}} <br>
																									<span style="font-size: 5pt;"> {!! $q->user_id !!} | {!! $q->stanica_id !!}</span></td>
																									<td>{!! $q->broj_zivotinje !!}</td>
																									<td>{!! $q->datum_pregled->toFormattedDateString() !!}</td>
																									<td>{!! $q->tip_pregled !!} <br> {!! $q->naziv_pregled !!}</td>
																									<td>{!! $q->rezultat_pregled !!}</td>
																									<td>{!! $q->imanje_ime !!}</td>
{{-- 																									<td>{!! $q->imanje_mjesto !!}</td>
																									<td>{!! $q->imanje_adresa !!}</td> --}}
																									<td> <a href="/pregledi/{{ $q->id }}" class="btn btn-sm green"> Pregled </a> </td>
																								</tr>
																							@endforeach

																					</tbody>
																				</table>





																			{{-- 	{{show_dump($authUser)}}
																				{{show_dump($data)}} --}}
													</div>
{{-- 													<div class="tab-pane" id="portlet_tab2">
														<p>
															 Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo.
														</p>
													</div>
													<div class="tab-pane" id="portlet_tab3">
														<p>
															 Ut wisi enim ad btn-smm veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat. Duis autem vel eum iriure dolor in hendrerit in vulputate velit esse molestie consequat, vel illum dolore eu feugiat nulla facilisis at vero eros et accumsan et iusto odio dignissim qui blandit praesent luptatum zzril delenit augue duis dolore te feugait nulla facilisi.
														</p>
													</div> --}}
												</div>
											</div>
										</div>

				</div>
			</div>


			<!-- END PAGE CONTENT INNER -->


		</div>
	</div>

@stop

<script>

</script>
