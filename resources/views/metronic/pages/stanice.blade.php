@extends ('metronic')
@include ('metronic.parts.ns_modal')


@section('page_level')
	<link rel="stylesheet" type="text/css" href="{{ url('metronic') }}/assets/global/plugins/select2/select2.css"/>
	<link rel="stylesheet" type="text/css" href="{{ url('metronic') }}/assets/global/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.css"/>
	<link href="{{ url('metronic') }}/assets/global/plugins/bootstrap-modal/css/bootstrap-modal-bs3patch.css" rel="stylesheet" type="text/css"/>
	<link href="{{ url('metronic') }}/assets/global/plugins/bootstrap-modal/css/bootstrap-modal.css" rel="stylesheet" type="text/css"/>
@stop

@section('page_plugins')
	<script type="text/javascript" src="{{ url('metronic') }}/assets/global/plugins/select2/select2.min.js"></script>
	<script type="text/javascript" src="{{ url('metronic') }}/assets/global/plugins/datatables/media/js/jquery.dataTables.min.js"></script>
	<script type="text/javascript" src="{{ url('metronic') }}/assets/global/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.js"></script>
	<script src="{{ url('metronic') }}/assets/admin/pages/scripts/table-managed.js"></script>
	
	<script src="{{ url('metronic') }}/assets/global/plugins/bootstrap-modal/js/bootstrap-modalmanager.js" type="text/javascript"></script>
	<script src="{{ url('metronic') }}/assets/global/plugins/bootstrap-modal/js/bootstrap-modal.js" type="text/javascript"></script>


		<script>
				jQuery(document).ready(function() {       
				//    Metronic.init(); // init metronic core components
				// Layout.init(); // init current layout
				// Demo.init(); // init demo features
				   TableManaged.init();
				});

		</script>
	
@stop

@section('content')

<div class="page-content-wrapper">
		<div class="page-content">
			<!-- BEGIN PAGE HEAD -->
<!-- 			<div class="page-head">

{{-- @include('flash::message') --}}
	BEGIN PAGE TITLE
	<div class="page-title">
		<h1>Dashboard <small>statistics & reports</small></h1>
	</div>
	END PAGE TITLE

</div> -->
			<!-- END PAGE HEAD -->
			<!-- BEGIN PAGE BREADCRUMB -->

			<!-- END PAGE BREADCRUMB -->
			<!-- BEGIN PAGE CONTENT INNER -->

			<div class="row">
				<div class="col-md-12">
	



					<div class="portlet box green tabbable">
											<div class="portlet-title">
												<div class="caption">
													<i class="fa fa-gift"></i>Veterinarske stanice

													<a class="btn blue" data-toggle="modal" href="#responsive"> Nova stanica </a>

												</div>
												<ul class="nav nav-tabs">
													<li>
{{-- 														<a href="#portlet_tab3" data-toggle="tab">
														Tab 3 </a> --}}
													</li>
											{{-- 		<li>
														<a href="#portlet_tab2" data-toggle="tab">
														Tab 2 </a>
													</li>
													<li class="active">
														<a href="#portlet_tab1" data-toggle="tab">
														Tab 1 </a>
													</li> --}}
												</ul>
											</div>





											<div class="portlet-body">
												<div class="tab-content">
													<div class="tab-pane active" id="portlet_tab1">
														<table class="table table-striped table-bordered table-hover" id="sample_1">
																					<thead>
																					<tr>
																						<th class="table-checkbox">
																							<input type="checkbox" class="group-checkable" data-set="#sample_1 .checkboxes"/>
																						</th>
																						<th>
																							 Stanica
																						</th>
																						<th>
																							 Mjesto
																						</th>
																						<th>
																							 Adresa
																						</th>
																						<th>
																							 Vlasnistvo
																						</th>
																						<th>
																							 Telefon
																						</th>
																						<th>
																							 Akcije
																						</th>
																		
																					</tr>
																					</thead>
																					<tbody>
																							@foreach ($stanice as $q)
																																												
																								<tr class="odd gradeX">
																									<td>
																										<input type="checkbox" class="checkboxes" value="1"/>
																									</td>
																									<td>
																										 {!! link_to("stanice/$q->id", $q->stanica) !!}
																									</td>
																									<td>
																										 {!! $q->mjesto !!}
																										
																									</td>
																									<td>
																										 {!! ($q->adresa) !!}
																										
																									</td>
																									<td>
																										 {!! ($q->vlasnistvo) !!}
																										 
																									</td>
																									<td>
																										 {!! ($q->telefon) !!}																										 
																									</td>
																									<td>
																										 <a href="/stanice/{{$q->id}}/delete" class="btn btn-sm red"> X </a>																									 
																									</td>
																								
																								</tr>
																							@endforeach

																					</tbody>
																				</table>

					{{show_dump($stanice)}}

													</div>
{{-- 													<div class="tab-pane" id="portlet_tab2">
														<p>
															 Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo.
														</p>
													</div>
													<div class="tab-pane" id="portlet_tab3">
														<p>
															 Ut wisi enim ad btn-smm veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat. Duis autem vel eum iriure dolor in hendrerit in vulputate velit esse molestie consequat, vel illum dolore eu feugiat nulla facilisis at vero eros et accumsan et iusto odio dignissim qui blandit praesent luptatum zzril delenit augue duis dolore te feugait nulla facilisi.
														</p>
													</div> --}}
												</div>
											</div>
										</div>

				</div>
			</div>


			<!-- END PAGE CONTENT INNER -->


		</div>
	</div>

@stop

<script>
	
</script>