@extends ('metronic')
			{{-- {{ dump(Request::is('create')) }} --}}


@section('page_level')
	<link rel="stylesheet" type="text/css" href="{{ url('metronic') }}/assets/global/plugins/select2/select2.css"/>
	<link rel="stylesheet" type="text/css" href="{{ url('metronic') }}/assets/global/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.css"/>
@stop

@section('page_plugins')
	<script type="text/javascript" src="{{ url('metronic') }}/assets/global/plugins/select2/select2.min.js"></script>
	<script type="text/javascript" src="{{ url('metronic') }}/assets/global/plugins/datatables/media/js/jquery.dataTables.min.js"></script>
	<script type="text/javascript" src="{{ url('metronic') }}/assets/global/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.js"></script>
	<script src="{{ url('metronic') }}/assets/admin/pages/scripts/table-managed.js"></script>

		<script>
				jQuery(document).ready(function() {
				//    Metronic.init(); // init metronic core components
				// Layout.init(); // init current layout
				// Demo.init(); // init demo features
				   TableManaged.init();

				    $(".select2").select2();
				});

		</script>

@stop

@section('content')

			<div class="page-content-wrapper">
					<div class="page-content">
							<div class="col-md-12">
									{{-- {{ dd(Request::path()) }} --}}

						<div class="portlet box green tabbable">
							<div class="portlet-title">
								<div class="caption">


								</div>

							</div>

							<div class="portlet-body">


								{!! BootForm::open()->action("$link")->post() !!}

								{!! BootForm::textArea('txt', 'txt') !!}

								{!! BootForm::submit('save')->class('btn blue') !!}
								{!! BootForm::close() !!}
							</div>

						</div>


							</div>
						</div>
					</div>







</div>


@stop
