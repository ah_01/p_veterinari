@extends ('metronic')


@section('page_level')
	<link rel="stylesheet" type="text/css" href="{{ url('metronic') }}/assets/global/plugins/select2/select2.css"/>
	<link rel="stylesheet" type="text/css" href="{{ url('metronic') }}/assets/global/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.css"/>
@stop

@section('page_plugins')
	<script type="text/javascript" src="{{ url('metronic') }}/assets/global/plugins/select2/select2.min.js"></script>
	<script type="text/javascript" src="{{ url('metronic') }}/assets/global/plugins/datatables/media/js/jquery.dataTables.min.js"></script>
	<script type="text/javascript" src="{{ url('metronic') }}/assets/global/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.js"></script>
	<script src="{{ url('metronic') }}/assets/admin/pages/scripts/table-managed.js"></script>


		<script>
				jQuery(document).ready(function() {       
				//    Metronic.init(); // init metronic core components
				// Layout.init(); // init current layout
				// Demo.init(); // init demo features
				   TableManaged.init();
				});

		</script>
	
@stop

@section('content')

<div class="page-content-wrapper">
		<div class="page-content">
			<!-- BEGIN PAGE HEAD -->
<!-- 			<div class="page-head">

	BEGIN PAGE TITLE
	<div class="page-title">
		<h1>Dashboard <small>statistics & reports</small></h1>
	</div>
	END PAGE TITLE

			<!-- BEGIN PAGE CONTENT INNER -->
			<div class="row ">

				<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
						

				<div class="portlet light">
										<div class="portlet-title">
											<div class="caption font-red-sunglo">
												<i class="icon-settings font-red-sunglo"></i>
												<span class="caption-subject bold uppercase"> Dodaj task</span>
											</div>

										</div>
										<div class="portlet-body form row">
											{!!BootForm::open()->action('/tasks')!!}
												<div class="col-md-6">
													{!!BootForm::text('Ime', 'name')!!}
												</div>
												<div class="col-md-6">
													{!!BootForm::text('Type', 'type')!!}
													
												</div>
												<div class="col-md-6">
													{!!BootForm::text('Dokument', 'file')!!}
													
												</div>
												<div class="col-md-6">
													{!!BootForm::submit('Submit')!!}
												</div>



											{!!BootForm::close()!!}


										</div>
									</div>





				</div>


			</div>
			<div class="row">
				<div class="col-md-12">
	

					{{-- {{dump($questions->keyBy('media'))}} --}}


	

					<div class="portlet box green tabbable">
											<div class="portlet-title">
												<div class="caption">
													<i class="fa fa-gift"></i>Taskovi
												</div>
												<ul class="nav nav-tabs">
													{{-- <li>
														<a href="#portlet_tab3" data-toggle="tab">
														Tab 3 </a>
													</li> --}}
													<li>
														<a href="#portlet_tab2" data-toggle="tab">
														Zatvoreni </a>
													</li>
													<li class="active">
														<a href="#portlet_tab1" data-toggle="tab">
														Otvoreni </a>
													</li>
												</ul>
											</div>





											<div class="portlet-body">
												<div class="tab-content">
													<div class="tab-pane active" id="portlet_tab1">
													Samo otvoreni taskovi
														<table class="table table-striped table-bordered table-hover" id="tasks">
																					<thead>
																					<tr>
																						<th class="table-checkbox">
																							<input type="checkbox" class="group-checkable" data-set="#sample_1 .checkboxes"/>
																						</th>
																						<th>
																							 Task
																						</th>
																						<th>
																							 Status
																						</th>
																						<th>
																							 Akcije
																						</th>
																						<th></th>
																						
																					</tr>
																					</thead>
																					<tbody>
																						@foreach ($open as $task)
																							<tr>
																								<td></td>
																								<td>{{$task->name}}</td>
																								<td>{{$task->status}}</td>
																								<td>

																									<div class="col-md-3">
																										{!!BootForm::open()->action("/tasks/$task->id")->delete()!!}
																										{!! BootForm::submit('Delete')->class('btn red-haze btn-sm') !!}
																										{!!BootForm::close()!!}

																									</div>
																									<div class="col-md-3">
																										{!!BootForm::open()->action("/tasks/$task->id")->put()!!}
																										{!! BootForm::submit('Zatvori')->class('btn green-haze btn-sm') !!}
																										{!!BootForm::close()!!}

																									</div>

																									</td>
																								<td> 
																									<p class="small">{{ $task->created_at->toDayDateTimeString()}} | {{ $task->updated_at->toDayDateTimeString()}}</p>
																								</td>
																							</tr>
																						@endforeach

																					</tbody>
																				</table>
													</div>
													<div class="tab-pane" id="portlet_tab2">
														<p>
															 izlistaj sve taskove
															 <table class="table table-striped table-bordered table-hover" id="sample_3">
															 							<thead>
															 							<tr>
															 								<th class="table-checkbox">
															 									<input type="checkbox" class="group-checkable" data-set="#sample_1 .checkboxes"/>
															 								</th>
															 								<th>
															 									 Task
															 								</th>
															 								<th>
															 									 Status
															 								</th>
															 								<th>
															 									 Akcije
															 								</th>
															 								<th></th>
															 								
															 							</tr>
															 							</thead>
															 							<tbody>
															 								@foreach ($all as $task)
															 									<tr>
															 										<td></td>
															 										<td>{{$task->name}}</td>
															 										<td>{{$task->status}}</td>
															 										<td></td>
															 										<td>
															 											<p class="small">
															 												{{ $task->created_at->toDayDateTimeString()}} 
															 												| 
															 												{{ $task->updated_at->toDayDateTimeString()}} 
															 											</p>
															 										</td>
															 									</tr>
															 								@endforeach

															 							</tbody>
															 						</table>
														</p>
													</div>
													<div class="tab-pane" id="portlet_tab3">
														<p>
															 Ut wisi enim ad btn-smm veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat. Duis autem vel eum iriure dolor in hendrerit in vulputate velit esse molestie consequat, vel illum dolore eu feugiat nulla facilisis at vero eros et accumsan et iusto odio dignissim qui blandit praesent luptatum zzril delenit augue duis dolore te feugait nulla facilisi.
														</p>
													</div>
												</div>
											</div>
										</div>

				</div>
			</div>


			<!-- END PAGE CONTENT INNER -->


		</div>
	</div>

@stop

<script>
	
</script>