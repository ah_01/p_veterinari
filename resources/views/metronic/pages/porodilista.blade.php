@extends ('metronic')


@section('page_level')
	<link rel="stylesheet" type="text/css" href="{{ url('metronic') }}/assets/global/plugins/select2/select2.css"/>
	<link rel="stylesheet" type="text/css" href="{{ url('metronic') }}/assets/global/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.css"/>
@stop

@section('page_plugins')
	<script type="text/javascript" src="{{ url('metronic') }}/assets/global/plugins/select2/select2.min.js"></script>
	<script type="text/javascript" src="{{ url('metronic') }}/assets/global/plugins/datatables/media/js/jquery.dataTables.min.js"></script>
	<script type="text/javascript" src="{{ url('metronic') }}/assets/global/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.js"></script>
	<script src="{{ url('metronic') }}/assets/admin/pages/scripts/table-managed.js"></script>

		<script>
				jQuery(document).ready(function() {       
				//    Metronic.init(); // init metronic core components
				// Layout.init(); // init current layout
				// Demo.init(); // init demo features
				   TableManaged.init();
				});

		</script>
	
@stop

@section('content')

<div class="page-content-wrapper">
		<div class="page-content">

			<div class="row">
				<div class="col-md-10">
	
@include('flash::message')
					<div class="portlet box green tabbable">
											<div class="portlet-title">
												<div class="caption">
													<i class="fa fa-gift"></i>Otvorena pitanja
												</div>
												<ul class="nav nav-tabs">
													<li>
														<a href="#portlet_tab3" data-toggle="tab">
														Tab 3 </a>
													</li>
													<li>
														<a href="#portlet_tab2" data-toggle="tab">
														Tab 2 </a>
													</li>
													<li class="active">
														<a href="#portlet_tab1" data-toggle="tab">
														Tab 1 </a>
													</li>
												</ul>
											</div>





											<div class="portlet-body">
												<div class="tab-content">
													<div class="tab-pane active" id="portlet_tab1">
													
													<a href="{{url('mame/exp')}}" target="_blank"> Preuzmi dokument </a>
													<hr>
														@include('metronic.parts.porodiliste_file')
													</div>
													<div class="tab-pane" id="portlet_tab2">
													<div class="container">
														
														<a href="{{url('mame/pdf')}}" target="_blank"> <!-- //mame/test -->
															<button type="button" class="btn btn-sm btn-info">Print Lables</button>
														</a>
														<a href="{{url('mame/close')}}">
															<button type="button" class="btn btn-sm btn-info">Zatvori rekorde</button>
														</a>

														<hr>


													</div>	
												<table class="table table-striped table-bordered table-hover" id="sample_1">
																					<thead>
																					<tr>
																					
																						<th>
																							 Ime
																						</th>
																						<th>
																							 Pol
																						</th>
																						<th>
																							 Grad
																						</th>
																						<th>
																							 Adresa
																						</th>
																		
																						<th>
																							 Status
																						</th>

																						<th>
																							 Akcije
																						</th>
																					</tr>
																					</thead>
																					<tbody>
																							@if ($result)
																								@foreach ($result as $q)
																																												
																								<tr class="odd gradeX">
																								
																									<td>
																										 {!! link_to("pitanja/$q->id", $q->ime) !!}
																									</td>
																									<td>
																										 {!! $q->pol !!}
																										
																									</td>
																									<td>
																										 {!! $q->grad !!}
																										
																									</td>
																									<td>
																										 {!!$q->adresa !!}
																			
																										 
																									</td>
																									<td class="center">
																									
																									@if ($q->status == "open")
																										<button type="button" class="btn btn-sm btn-danger">Otvoreno</button>
																									@else
																										<button type="button" class="btn btn-sm green">Zatvoreno</button>
																									@endif
																										 
																									</td>
																									<td class="center">
																									
																										Akcije
																										 
																									</td>
																								
																								</tr>
																							@endforeach
																							@endif

																					</tbody>
																				</table>


													</div>
													<div class="tab-pane" id="portlet_tab3">
														<p>
															 Ut wisi enim ad btn-smm veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat. Duis autem vel eum iriure dolor in hendrerit in vulputate velit esse molestie consequat, vel illum dolore eu feugiat nulla facilisis at vero eros et accumsan et iusto odio dignissim qui blandit praesent luptatum zzril delenit augue duis dolore te feugait nulla facilisi.
														</p>
													</div>
												</div>
											</div>
										</div>

				</div>

				<div class="col-md-2">
					

				<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
					<div class="dashboard-stat2">
						<div class="display">
							<div class="number">
								<h4 class="font-green-sharp">
									##############
								</h4>
								<small>Pitanja - mediji</small>
							</div>
							<div class="icon">
								<i class="icon-pie-chart"></i>
							</div>
						</div>
					</div>
				</div>
				<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
					<div class="dashboard-stat2">
						<div class="display">
							<div class="number">
								<h3 class="font-red-haze">##########</h3>
								<small class="font-red-haze">Pitanja treba paznju</small>
								<br>
								<br>
							</div>
							<div class="icon">
								<i class="icon-like font-red-haze"></i>
							</div>
						</div>
					</div>
				</div>



				</div>
			</div>


			<!-- END PAGE CONTENT INNER -->


		</div>
	</div>

@stop

<script>
	
</script>