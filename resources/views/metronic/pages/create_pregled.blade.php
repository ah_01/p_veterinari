@extends ('metronic')


@section('page_level')
	<link rel="stylesheet" type="text/css" href="{{ url('metronic') }}/assets/global/plugins/select2/select2.css"/>
	<link rel="stylesheet" type="text/css" href="{{ url('metronic') }}/assets/global/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.css"/>
	<link href="{{ url('metronic') }}/assets/global/plugins/bootstrap-modal/css/bootstrap-modal-bs3patch.css" rel="stylesheet" type="text/css"/>
	<link href="{{ url('metronic') }}/assets/global/plugins/bootstrap-modal/css/bootstrap-modal.css" rel="stylesheet" type="text/css"/>
	<link href="{{ url('metronic') }}/assets/global/css/components-rounded.css" rel="stylesheet" type="text/css"/>
	<link rel="stylesheet" type="text/css" href="{{ url('metronic') }}/assets/datepicker/css/datepicker.css"/>

@stop

@section('page_plugins')
	<script type="text/javascript" src="{{ url('metronic') }}/assets/global/plugins/select2/select2.min.js"></script>
	<script type="text/javascript" src="{{ url('metronic') }}/assets/global/plugins/datatables/media/js/jquery.dataTables.min.js"></script>
	<script type="text/javascript" src="{{ url('metronic') }}/assets/global/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.js"></script>
	<script src="{{ url('metronic') }}/assets/admin/pages/scripts/table-managed.js"></script>

	<script src="{{ url('metronic') }}/assets/global/plugins/bootstrap-modal/js/bootstrap-modalmanager.js" type="text/javascript"></script>
	<script src="{{ url('metronic') }}/assets/global/plugins/bootstrap-modal/js/bootstrap-modal.js" type="text/javascript"></script>
	<script src="{{ url('metronic') }}/assets/datepicker/js/bootstrap-datepicker.js"></script>

	<script type="text/javascript" src="{{ url('metronic') }}/assets/global/plugins/jquery-validation/js/jquery.validate.min.js"></script>
	<script type="text/javascript" src="{{ url('metronic') }}/assets/global/plugins/jquery-validation/js/additional-methods.min.js"></script>
	<script type="text/javascript" src="{{ url('metronic') }}/assets/global/plugins/bootstrap-wizard/jquery.bootstrap.wizard.min.js"></script>

	<script src="{{ url('metronic') }}/assets/admin/pages/scripts/form-wizard.js"></script>


		<script>
				jQuery(document).ready(function() {
				//    Metronic.init(); // init metronic core components
				// Layout.init(); // init current layout
				// Demo.init(); // init demo features
				   TableManaged.init();
					 FormWizard.init();
				   $(".select2").select2();

				   $('#datum_pregled').datepicker({
				   		format: 'yyyy-mm-dd'
				   });

				   $('#datum_nalaza').datepicker({
				   		format: 'yyyy-mm-dd'
				   });
				   // $("#zivotinja").select2(

				   // 	);

				  //  $('#kanton').change(function(){
				  //  			$.get("{{ url('api/kanton')}}",
				  //  				{ option: $(this).val() },
				  //  				function(data) {
				  //  					var imanje = $('#imanje');
				  //  					// console.log(data);
				  //  					$("#imanje").select2(
					 //
				  //  						);
				  //  					$('#imanje').empty().append("<option value='---'>---</option>");
				  //  					$('#zivotinja').empty().append("<option value='---'>---</option>");
				  //   				// return data;
				  //  			            // $('#imanje').append("<option value='---'>---</option>");
				  //  					$.each(data, function(index, element) {
				  //  						// console.log(element.id_broj);
				  //  			            $('#imanje').append("<option value='"+ element.id +"'>" + element.id_broj + "</option>");
				  //  			            // console.log(index + '|||||||' + element);
				  //  			        });
				  //  				});
				  //  		});
				  //  $('#imanje').change(function(){
				  //  			$.get("{{ url('api/zivotinje')}}",
				  //  				{ option: $(this).val() },
				  //  				function(data) {
				  //  					var zivotinja = $('#zivotinja');
				  //  					// console.log(data);
				  //  					// $("#zivotinja").select2(
					 //
				  //  					// 	);trazi
				  //  					// zivotinja.empty();
				  //   				// return data;
				  //  					$.each(data, function(index, element) {
				  //  						console.log(element);
				  //  			            $('#zivotinja').append("<option value='"+ index +"'>" + element + "</option>");
				  //  			            // console.log(index + '|||||||' + element);
				  //  			        });
				  //  				});
				  //  		});
				  //  $('#imanje').change(function(){
				  //  			$.get("{{ url('api/imanje')}}",
				  //  				{ option: $(this).val() },
				  //  				function(data) {
				  //  					// var zivotinja = $('#zivotinja');
				  //  					// console.log(data[0].ime);
				  //  					$("#imanje_ime1").val(data[0].ime);
				  //  					$("#imanje_adresa").val(data[0].adresa);
				  //  					$("#imanje_mjesto").val(data[0].mjesto);
				  //  					$("#imanje_pbroj").val(data[0].pbroj);
					 //
				  //  				});
				  //  		});


				});

		</script>

@stop

@section('content')

	<!-- BEGIN CONTENT -->
	<div class="page-content-wrapper">
		<div class="page-content">
			<!-- BEGIN SAMPLE PORTLET CONFIGURATION MODAL FORM-->
			<div class="modal fade" id="portlet-config" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
				<div class="modal-dialog">
					<div class="modal-content">
						<div class="modal-header">
							<button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
							<h4 class="modal-title">Modal title</h4>
						</div>
						<div class="modal-body">
							 Widget settings form goes here
						</div>
						<div class="modal-footer">
							<button type="button" class="btn blue">Save changes</button>
							<button type="button" class="btn default" data-dismiss="modal">Close</button>
						</div>
					</div>
					<!-- /.modal-content -->
				</div>
				<!-- /.modal-dialog -->
			</div>
			<!-- /.modal -->
			<!-- END SAMPLE PORTLET CONFIGURATION MODAL FORM-->
			<!-- BEGIN PAGE HEADER-->


			<!-- END PAGE HEADER-->
			<!-- BEGIN PAGE CONTENT-->
			<div class="row">
				<div class="col-md-12">
					<div class="portlet box blue" id="form_wizard_1">
						<div class="portlet-title">
							<div class="caption">
								<i class="fa fa-gift"></i> Dodaj pregled - <span class="step-title">
								Korak 1 of 4 </span>
							</div>
						</div>
						<div class="portlet-body form">
							{{-- <form action="#" class="form-horizontal" id="submit_form" method="POST"> --}}
								<div class="form-wizard">

									{!! BootForm::open()->action('/pregledi/create')->id('submit_form')->class('form-horizontal')->method('POST') !!}

									{!! BootForm::hidden('stanica_id')->value($authUser->stanica->id) !!}
									{!! BootForm::hidden('imanje_id')->value($imanje->id) !!}


									<div class="form-body">
										<ul class="nav nav-pills nav-justified steps">
											<li>
												<a href="#tab1" data-toggle="tab" class="step">
												<span class="number">
												1 </span>
												<span class="desc">
												<i class="fa fa-check"></i> Pregled </span>
												</a>
											</li>
											<li>
												<a href="#tab2" data-toggle="tab" class="step">
												<span class="number">
												2 </span>
												<span class="desc">
												<i class="fa fa-check"></i> Zivotinje </span>
												</a>
											</li>
											{{-- <li>
												<a href="#tab3" data-toggle="tab" class="step active">
												<span class="number">
												3 </span>
												<span class="desc">
												<i class="fa fa-check"></i> Rezultati </span>
												</a>
											</li> --}}
											<li>
												<a href="#tab4" data-toggle="tab" class="step">
												<span class="number">
												3 </span>
												<span class="desc">
												<i class="fa fa-check"></i> Potvrda </span>
												</a>
											</li>
										</ul>
										<div id="bar" class="progress progress-striped" role="progressbar">
											<div class="progress-bar progress-bar-success">
											</div>
										</div>

										<div class="tab-content">
											<div class="alert alert-danger display-none">
												<button class="close" data-dismiss="alert"></button>
												You have some form errors. Please check below.
											</div>
											<div class="alert alert-success display-none">
												<button class="close" data-dismiss="alert"></button>
												Your form validation is successful!
											</div>



											<div class="tab-pane active" id="tab1">
												<h3 class="block">Informacije o pregledu</h3>

												<h4>Imanje broj: {{ ($imanje->id_broj) }}</h4>

												<div class="form-group">
													<label class="control-label col-md-3">Datum pregleda <span class="required">
													* </span>
													</label>

													<div class="col-md-4">
														<input type="text" class="form-control datum_pregled" name="datum_pregled" id="datum_pregled"/>
														<span class="help-block">
														Unesite datum </span>
													</div>
												</div>

												<div class="form-group">
													<label class="control-label col-md-3">Naziv pregleda <span class="required">
													* </span>
													</label>

													<div class="col-md-4">
														<input type="text" class="form-control datum_pregled" name="naziv_pregled" id="naziv_pregled"/>
														<span class="help-block">
														Unesite naziv pregleda </span>
													</div>
												</div>

												<div class="form-group">
													<label class="control-label col-md-3">Tip pregleda<span class="required">
													* </span>
													</label>
													<div class="col-md-4">

														<select class="form-control tip_pregled" name="tip_pregled" id="tip_pregled">
															<option value=""></option>
															<option value="---">---</option>
															<option value="pregled">Pregled</option>
															<option value="vakcinacija">Vakcinacija</option>
														</select>
														<span class="help-block">
														Tip pregleda </span>
													</div>
												</div>

											</div>



											{{-- ******************************* TAB 2 ******************************* --}}
											<div class="tab-pane" id="tab2">
												<h3 class="block">Unesite informacije o zivotinjama</h3>
												{{-- */  $zivotinje = $imanje->zivotinje->groupBy('status_zivotinje') /*  --}}

												<div class="row">
													<div class="col-md-5">
														<div class="form-group">
															<label class="control-label col-md-2 text-left">Aktivne zivotinje <span class="required">
															* </span>
															</label>
															<div class="col-md-10">
																	@foreach($zivotinje["A"] as $zivotinja)
																		<label for="zivotinje[]"><input type="checkbox" class="form-control" name="zivotinje[]" value="{{ $zivotinja->broj_zivotinje }}"/> {{ $zivotinja->broj_zivotinje }} </label> <br>
																	@endforeach
																		<span class="help-block">
																		Selektujte zivotinje </span>
															</div>
														</div>

													</div>
													<div class="col-md-7">

														<div class="form-group">
															<label class="control-label col-md-3">Rezultat <span class="required">
															* </span>
															</label>
															<div class="col-md-4">

																<select class="form-control" name="rezultat_pregled" id="rezultat_pregled">

																	<option value="">Select</option>
																	<option value="poz(+)">poz(+)</option>
																	<option value="neg(-)">neg(-)</option>

																</select>

																<span class="help-block">
																Unesite rezultat pregleda </span>
															</div>
														</div>

														<div class="form-group">
															<label class="control-label col-md-3">Broj nalaza <span class="required">
															* </span>
															</label>
															<div class="col-md-4">
																<input type="text" class="form-control" name="broj_nalaza"/>
																<span class="help-block">
																Unesite broj nalaza </span>
															</div>
														</div>

														<div class="form-group">
															<label class="control-label col-md-3">Datum nalaza <span class="required">
															* </span>
															</label>
															<div class="col-md-4">
																<input type="text" class="form-control" name="datum_nalaza" id="datum_nalaza"/>
																<span class="help-block">
																Unesite datum nalaza </span>
															</div>
														</div>

														<div class="form-group">
															<label class="control-label col-md-3">Oznaka vakcine <span class="required">
															* </span>
															</label>
															<div class="col-md-4">
																<input type="text" class="form-control" name="naziv_oznaka_vakcine" id="naziv_oznaka_vakcine"/>
																<span class="help-block">
																Unesite oznaku vakcine ako se radi o vakcinisanju zivotinje </span>
															</div>
														</div>

													</div>


												</div>
												</div>



											{{-- ******************************* TAB 3 ******************************* --}}
											{{-- <div class="tab-pane" id="tab3">
												<h3 class="block">Potvrdite Vas unos </h3>


													<h4 class="form-section">Pregled</h4>
													<div class="form-group">
														<label class="control-label col-md-3">datum pregleda:</label>
														<div class="col-md-4">
															<p class="form-control-static" data-display="datum_pregled">
															</p>
														</div>
													</div>

													<div class="form-group">
														<label class="control-label col-md-3">Username1:</label>
														<div class="col-md-4">
															<p class="form-control-static" data-display="datum_pregled">
															</p>
														</div>
													</div>

											</div> --}}



											{{-- ******************************* TAB 4 ******************************* --}}

											<div class="tab-pane" id="tab4">
												<h3 class="block">Potvrdite informacije</h3>
												<h4 class="form-section">Pregled</h4>
												<div class="form-group">
													<label class="control-label col-md-3">Datum pregled:</label>
													<div class="col-md-4">
														<p class="form-control-static" data-display="datum_pregled">
														</p>
													</div>
												</div>
												<div class="form-group">
													<label class="control-label col-md-3">Tip pregleda:</label>
													<div class="col-md-4">
														<p class="form-control-static" data-display="tip_pregled">
														</p>
													</div>
												</div>

												<div class="form-group">
													<label class="control-label col-md-3">Naziv pregleda:</label>
													<div class="col-md-4">
														<p class="form-control-static" data-display="naziv_pregled">
														</p>
													</div>
												</div>


												</div>
											</div>
										</div>
									</div>
									<div class="form-actions">
										<div class="row">
											<div class="col-md-offset-3 col-md-9">
												<a href="javascript:;" class="btn default button-previous">
												<i class="m-icon-swapleft"></i> Back </a>
												<a href="javascript:;" class="btn blue button-next">
												Continue <i class="m-icon-swapright m-icon-white"></i>
												</a>
												<a href="javascript:;" class="btn green button-submit">
												Submit <i class="m-icon-swapright m-icon-white"></i>
												</a>
											</div>
										</div>
									</div>
								</div>
							{!! BootForm::close() !!}

							{{-- </form> --}}
						</div>
					</div>
				</div>
			</div>
			<!-- END PAGE CONTENT-->
		</div>
	</div>
	<!-- END CONTENT -->

@stop

<script>

</script>
