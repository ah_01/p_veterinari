@extends ('metronic')


@section('page_level')
	<link rel="stylesheet" type="text/css" href="{{ url('metronic') }}/assets/global/plugins/select2/select2.css"/>
	<link rel="stylesheet" type="text/css" href="{{ url('metronic') }}/assets/global/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.css"/>
@stop

@section('page_plugins')
	<script type="text/javascript" src="{{ url('metronic') }}/assets/global/plugins/select2/select2.min.js"></script>
	<script type="text/javascript" src="{{ url('metronic') }}/assets/global/plugins/datatables/media/js/jquery.dataTables.min.js"></script>
	<script type="text/javascript" src="{{ url('metronic') }}/assets/global/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.js"></script>
	<script src="{{ url('metronic') }}/assets/admin/pages/scripts/table-managed.js"></script>


		<script>
				jQuery(document).ready(function() {       
				//    Metronic.init(); // init metronic core components
				// Layout.init(); // init current layout
				// Demo.init(); // init demo features
				   TableManaged.init();
				});

		</script>
	
@stop

@section('content')

<div class="page-content-wrapper">
		<div class="page-content">
			<!-- BEGIN PAGE HEAD -->
<!-- 			<div class="page-head">

{{-- @include('flash::message') --}}
	BEGIN PAGE TITLE
	<div class="page-title">
		<h1>Dashboard <small>statistics & reports</small></h1>
	</div>
	END PAGE TITLE

</div> -->
			<!-- END PAGE HEAD -->
			<!-- BEGIN PAGE BREADCRUMB -->
{{-- 			<ul class="page-breadcrumb breadcrumb hide">
				<li>
					<a href="javascript:;">Home</a><i class="fa fa-circle"></i>
				</li>
				<li class="active">
					 Dashboard
				</li>
			</ul> --}}
			<!-- END PAGE BREADCRUMB -->
			<!-- BEGIN PAGE CONTENT INNER -->
			<div class="row ">
				<div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
					<div class="dashboard-stat2">
						<div class="display">
							<div class="number">
								<h4 class="font-green-sharp">
									<?php 
										$qo = $questions->groupBy('media');
										$qo->each(function ($item, $key)
										{
											echo ($key . ' : ' . $item->count() . '<br>');
										})
									 ?>
								</h4>
								<small>Pitanja - mediji</small>
							</div>
							<div class="icon">
								<i class="icon-pie-chart"></i>
							</div>
						</div>
{{-- 						<div class="progress-info">
							<div class="progress">
								<span style="width: 76%;" class="progress-bar progress-bar-success green-sharp">
								<span class="sr-only">76% progress</span>
								</span>
							</div>
							<div class="status">
								<div class="status-title">
									 progress
								</div>
								<div class="status-number">
									 76%
								</div>
							</div>
						</div> --}}
					</div>
				</div>
				<div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
					<div class="dashboard-stat2">
						<div class="display">
							<div class="number">
								<h3 class="font-red-haze">{{ $m['due'] }}</h3>
								<small class="font-red-haze">Pitanja treba paznju</small>
								<br>
								<br>
							</div>
							<div class="icon">
								<i class="icon-like font-red-haze"></i>
							</div>
						</div>
{{-- 						<div class="progress-info">
							<div class="progress">
								<span style="width: 85%;" class="progress-bar progress-bar-success red-haze">
								<span class="sr-only">85% change</span>
								</span>
							</div>
							<div class="status">
								<div class="status-title">
									 change
								</div>
								<div class="status-number">
									 85%
								</div>
							</div>
						</div> --}}
					</div>
				</div>
				<div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
				<a href="{{ url('pitanja/create') }}" class="btn blue">
					<i class="fa fa-file-o"></i> Novo pitanje 
				</a>
{{-- 					<div class="dashboard-stat2">
						<div class="display">
							<div class="number">
								<h3 class="font-blue-sharp">567</h3>
								<small>NEW ORDERS</small>
							</div>
							<div class="icon">
								<i class="icon-basket"></i>
							</div>
						</div>
						<div class="progress-info">
							<div class="progress">
								<span style="width: 45%;" class="progress-bar progress-bar-success blue-sharp">
								<span class="sr-only">45% grow</span>
								</span>
							</div>
							<div class="status">
								<div class="status-title">
									 grow
								</div>
								<div class="status-number">
									 45%
								</div>
							</div>
						</div>
					</div> --}}
				</div>
				<div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
{{-- 					<div class="dashboard-stat2">
						<div class="display">
							<div class="number">
								<h3 class="font-purple-soft">276</h3>
								<small>NEW USERS</small>
							</div>
							<div class="icon">
								<i class="icon-user"></i>
							</div>
						</div>
						<div class="progress-info">
							<div class="progress">
								<span style="width: 57%;" class="progress-bar progress-bar-success purple-soft">
								<span class="sr-only">56% change</span>
								</span>
							</div>
							<div class="status">
								<div class="status-title">
									 change
								</div>
								<div class="status-number">
									 57%
								</div>
							</div>
						</div>
					</div> --}}
				</div>
			</div>
			<div class="row">
				<div class="col-md-12">
	

					{{-- {{dump($questions->keyBy('media'))}} --}}


	

					<div class="portlet box green tabbable">
											<div class="portlet-title">
												<div class="caption">
													<i class="fa fa-gift"></i>Otvorena pitanja
												</div>
												<ul class="nav nav-tabs">
													<li>
														<a href="#portlet_tab3" data-toggle="tab">
														Tab 3 </a>
													</li>
													<li>
														<a href="#portlet_tab2" data-toggle="tab">
														Tab 2 </a>
													</li>
													<li class="active">
														<a href="#portlet_tab1" data-toggle="tab">
														Tab 1 </a>
													</li>
												</ul>
											</div>





											<div class="portlet-body">
												<div class="tab-content">
													<div class="tab-pane active" id="portlet_tab1">
														<table class="table table-striped table-bordered table-hover" id="sample_1">
																					<thead>
																					<tr>
																						<th class="table-checkbox">
																							<input type="checkbox" class="group-checkable" data-set="#sample_1 .checkboxes"/>
																						</th>
																						<th>
																							 Pitanje
																						</th>
																						<th>
																							 Media
																						</th>
																						<th>
																							 Datum
																						</th>
																						<th>
																							 Dana
																						</th>
																		
																						<th>
																							 Status
																						</th>
																					</tr>
																					</thead>
																					<tbody>
																							@foreach ($questions as $q)
																																												
																								<tr class="odd gradeX">
																									<td>
																										<input type="checkbox" class="checkboxes" value="1"/>
																									</td>
																									<td>
																										 {!! link_to("pitanja/$q->id", $q->title) !!}
																									</td>
																									<td>
																										 {!! $q->media !!}
																										
																									</td>
																									<td>
																										 {!! ($q->created_at->toFormattedDateString()) !!}
																										
																									</td>
																									<td>
																										@if ($q->status == 0)
																											 <span class="badge <?php echo ($q->days>4)? 'badge-danger' : 'badge-info'; ?>"> 
																											 	{!! $q->days !!}
																											 </span>
																										@endif
																										 
																									</td>
																									<td class="center">
																									@if ($q->status == 0)
																										<button type="button" class="btn btn-sm btn-danger">Otvoreno</button>
																									@else
																										<button type="button" class="btn btn-sm green">Zatvoreno</button>
																									@endif
																										 
																									</td>
																								
																								</tr>
																							@endforeach

																					</tbody>
																				</table>
													</div>
													<div class="tab-pane" id="portlet_tab2">
														<p>
															 Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo.
														</p>
													</div>
													<div class="tab-pane" id="portlet_tab3">
														<p>
															 Ut wisi enim ad btn-smm veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat. Duis autem vel eum iriure dolor in hendrerit in vulputate velit esse molestie consequat, vel illum dolore eu feugiat nulla facilisis at vero eros et accumsan et iusto odio dignissim qui blandit praesent luptatum zzril delenit augue duis dolore te feugait nulla facilisi.
														</p>
													</div>
												</div>
											</div>
										</div>

				</div>
			</div>


			<!-- END PAGE CONTENT INNER -->


		</div>
	</div>

@stop

<script>
	
</script>