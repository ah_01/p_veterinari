<div id="responsive" class="modal fade" tabindex="-1" data-width="760">
	<div class="modal-header">
		<button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
		<h4 class="modal-title">Dodaj novog korisnika</h4>
	</div>
	<div class="modal-body">
		<div class="row">
			<div class="col-md-12">
{{-- 			<div class="alert alert-danger">
				<strong>Info!</strong> Sekcija nije funkcionalna.
			</div> --}}
				{!! BootForm::open()->action('/korisnici/create')->method('POST') !!}
				{{-- {!! BootForm::hidden('user_id')->value(1) !!} --}}
				{!! BootForm::text('Korisnik', 'name') !!}
				{!! BootForm::text('Email', 'email') !!}
				{!! BootForm::text('Telefon', 'phone') !!}
				
				@if (getuser()->level == 1)
					{!! BootForm::text('Nivo: 1-admin, 2-ostali', 'level') !!}

				@endif

				{!! BootForm::password('password', 'password') !!}
				
				{{-- 
				{!! BootForm::select('Stanica', 'stanica_id')->options(
															[
																'salovi' => 'Salovi', 
																'jakne' => 'Jakne', 
																'kaputi' => 'Kaputi', 
																'pantole' => 'Pantole', 

															])->select('salovi')->class("form-control select2me"); !!}
 --}}
				

				{!! BootForm::submit('save')->class('btn blue') !!}
				{!! BootForm::close() !!}
			</div>

		</div>
	</div>
{{-- 	<div class="modal-footer">
		<button type="button" data-dismiss="modal" class="btn btn-default">Close</button>
		<button type="button" class="btn blue">Save changes</button>
	</div> --}}
</div>
<!-- END responsive