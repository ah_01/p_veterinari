@if (Session::has('flash_notification.message'))
			    <script>
			    jQuery(document).ready(function() {  
		    		$.bootstrapGrowl("{{ Session::get('flash_notification.message') }}", {
						  ele: '.alert', // which element to append to
						  type: '{{ Session::get('flash_notification.level') }}', // (null, 'info', 'danger', 'success')
						  offset: {from: 'top', amount: 450}, // 'top', or 'bottom'
						  align: 'center', // ('left', 'right', or 'center')
						  width: 313, // (integer, or 'auto')
						  delay: 4000, // Time while the message will be displayed. It's not equivalent to the *demo* timeOut!
						  allow_dismiss: true, // If true then will display a cross to close the popup.
						  stackup_spacing: 10 // spacing between consecutively stacked growls.
						});
		    	});
			    </script>
	@endif	