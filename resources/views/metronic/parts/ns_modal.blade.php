<div id="responsive" class="modal fade" tabindex="-1" data-width="760">
	<div class="modal-header">
		<button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
		<h4 class="modal-title">Dodaj novu stanicu</h4>
	</div>
	<div class="modal-body">
		<div class="row">
			<div class="col-md-12">
{{-- 			<div class="alert alert-danger">
				<strong>Info!</strong> Sekcija nije funkcionalna.
			</div> --}}
				{!! BootForm::open()->action('/stanice/create')->method('POST') !!}
					
					{!! BootForm::text('Stanica', 'stanica') !!}
					{!! BootForm::text('Kanton', 'kanton') !!}
					{!! BootForm::text('Mjesto', 'mjesto') !!}
					{!! BootForm::text('pbroj', 'pbroj') !!}
					{!! BootForm::text('adresa', 'adresa') !!}
					{!! BootForm::textarea('komentar', 'komentar') !!}
					{!! BootForm::text('telefon', 'telefon') !!}
					{!! BootForm::text('Email', 'email') !!}

					{!! BootForm::select('Vlasnistvo', 'vlasnistvo')->options([
							'javno'	=>	'Javno',
							'privatno'	=>	'Privatno',
					])->class('select2 form-control') !!}
			

				{!! BootForm::submit('save')->class('btn blue') !!}
				{!! BootForm::close() !!}
			</div>

		</div>
	</div>
{{-- 	<div class="modal-footer">
		<button type="button" data-dismiss="modal" class="btn btn-default">Close</button>
		<button type="button" class="btn blue">Save changes</button>
	</div> --}}
</div>
<!-- END responsive