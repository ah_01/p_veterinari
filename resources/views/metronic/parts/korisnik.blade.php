@extends ('metronic')
			{{-- {{ dump(Request::is('create')) }} --}}


@section('page_level')
	<link rel="stylesheet" type="text/css" href="{{ url('metronic') }}/assets/global/plugins/select2/select2.css"/>
	<link rel="stylesheet" type="text/css" href="{{ url('metronic') }}/assets/global/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.css"/>
@stop

@section('page_plugins')
	<script type="text/javascript" src="{{ url('metronic') }}/assets/global/plugins/select2/select2.min.js"></script>
	<script type="text/javascript" src="{{ url('metronic') }}/assets/global/plugins/datatables/media/js/jquery.dataTables.min.js"></script>
	<script type="text/javascript" src="{{ url('metronic') }}/assets/global/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.js"></script>
	<script src="{{ url('metronic') }}/assets/admin/pages/scripts/table-managed.js"></script>

		<script>
				jQuery(document).ready(function() {       
				//    Metronic.init(); // init metronic core components
				// Layout.init(); // init current layout
				// Demo.init(); // init demo features
				   TableManaged.init();

				    $(".select2").select2();
				});

		</script>
	
@stop

@section('content')
			
			<div class="page-content-wrapper">
					<div class="page-content">
							<div class="col-md-12">
									{{-- {{ dd(Request::path()) }} --}}

						<div class="portlet box green tabbable">
							<div class="portlet-title">
								<div class="caption">
				
								
								</div>
								
							</div>

							<div class="portlet-body">
								
							@if (Request::is('*/create') == true)
								{!! BootForm::open()->action("/korisnici/create")->post() !!}
							@else
								{!! BootForm::open()->action("/korisnici/$korisnik->id/edit", $korisnik)->put() !!}
								{!! BootForm::bind($korisnik) !!}
							@endif

								
								{!! BootForm::text('Korisnik', 'name') !!}
								{!! BootForm::text('Email', 'email') !!}
								{!! BootForm::text('Telefon', 'phone') !!}

								
								{!! BootForm::select('Stanica', 'stanica_id')->options($korisnik->stanice)->class("form-control select2me"); !!}
								
								@if ($korisnik->stanica_id != null)


									<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 btn green">
									{!! BootForm::text('Stanica', 'stanica')->value($korisnik->stanica->stanica) !!}
										<div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
											{!! BootForm::text('mjesto', 'mjesto')->value($korisnik->stanica->mjesto) !!}
												
										</div>
										<div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
											{!! BootForm::text('pbroj', 'pbroj')->value($korisnik->stanica->pbroj) !!}
												
										</div>
										<div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
											{!! BootForm::text('adresa', 'adresa')->value($korisnik->stanica->adresa) !!}
												
										</div>
										<div class="col-lg-3 col-md-3 col-sm-6 col-xs-12 ">
											{!! BootForm::text('telefon', 'telefon')->value($korisnik->stanica->telefon) !!}
												
										</div>
									</div>
									
								@endif

								{!! BootForm::password("password::$korisnik->password", 'password') !!}
								
								{{-- 
								{!! BootForm::select('Stanica', 'stanica_id')->options(
																			[
																				'salovi' => 'Salovi', 
																				'jakne' => 'Jakne', 
																				'kaputi' => 'Kaputi', 
																				'pantole' => 'Pantole', 

																			])->select('salovi')->class("form-control select2me"); !!}
 --}}
								<hr>


								<hr>
								{!! BootForm::submit('save')->class('btn blue') !!}
								{!! BootForm::close() !!}
							</div>
								
						</div>

									{{ show_dump($korisnik) }}
							

							</div>
						</div>
					</div>





								{{show_dump($korisnik->stanica)}}


</div>


@stop


