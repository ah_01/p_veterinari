@extends ('metronic')
			{{-- {{ dump(Request::is('create')) }} --}}


@section('page_level')
	<link rel="stylesheet" type="text/css" href="{{ url('metronic') }}/assets/global/plugins/select2/select2.css"/>
	<link rel="stylesheet" type="text/css" href="{{ url('metronic') }}/assets/global/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.css"/>
	<link rel="stylesheet" type="text/css" href="{{ url('metronic') }}/assets/datepicker/css/datepicker.css"/>
@stop

@section('page_plugins')
	<script type="text/javascript" src="{{ url('metronic') }}/assets/global/plugins/select2/select2.min.js"></script>
	<script type="text/javascript" src="{{ url('metronic') }}/assets/global/plugins/datatables/media/js/jquery.dataTables.min.js"></script>
	<script type="text/javascript" src="{{ url('metronic') }}/assets/global/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.js"></script>
	<script src="{{ url('metronic') }}/assets/admin/pages/scripts/table-managed.js"></script>
	<script src="{{ url('metronic') }}/assets/datepicker/js/bootstrap-datepicker.js"></script>

		
		<script>

				jQuery(document).ready(function() {       
				//    Metronic.init(); // init metronic core components
				// Layout.init(); // init current layout
				// Demo.init(); // init demo features
				   TableManaged.init();
				   $('#datum_pregled').datepicker({
				   		format: 'yyyy-mm-dd'
				   });

				   $('#datum_nalaza').datepicker({
				   		format: 'yyyy-mm-dd'
				   });

				    $(".select2").select2();
				    $(".pregled").select2(
				    	{tags: <?=$pregled->pregled_naziv; ?>}
				    	);
				});

		</script>
	
@stop

@section('content')
			
			<div class="page-content-wrapper">
					<div class="page-content">
							<div class="col-md-12">
									{{-- {{ dd(Request::path()) }} --}}
									{{-- {{ dd($pregled) }} --}}

						<div class="portlet box green tabbable">
							<div class="portlet-title">
								<div class="caption">
									Pregled imanje broj: {{$pregled->imanje_id_broj}}, zivotinja broj {{$pregled->broj_zivotinje}}
								
								</div>
								
							</div>

							<div class="portlet-body">

							<div class="row">
								<div class="col-lg-12">
								
								
										@if (Request::is('*/create') == true)
											{!! BootForm::open()->action("/pregledi/create")->post() !!}
										@else
											{!! BootForm::open()->action("/pregledi/$pregled->id/edit", $pregled)->put() !!}
											{!! BootForm::bind($pregled) !!}
										@endif
										<div class="col-lg-12 bg-blue">
													<div class="col-lg-6">
														{{-- {{$authUser->stanica}} --}}
														
														{!! BootForm::text('imanje_id_broj', 'imanje_id_broj')->id('imanje') !!}
														{{-- {!! BootForm::text('stanica_id', 'stanica_id')->value($authUser->stanica->stanica_id) !!} --}}
														{!! BootForm::hidden('stanica_id')->value($authUser->stanica->id) !!}
														{{-- {{dump($authUser->id)}} --}}
														{{-- {!! BootForm::text('korisnik_id', 'korisnik_id') !!} --}}
													</div>

													<div class="col-lg-6">
														{!! BootForm::text('imanje_ime', 'imanje_ime')->id('imanje_ime')->class("form-control select2me") !!}
														
													</div>

													<div class="col-lg-6">
														{!! BootForm::text('imanje_adresa', 'imanje_adresa')->id('imanje_adresa') !!}
													</div>

													<div class="col-lg-3">
														{!! BootForm::text('imanje_mjesto', 'imanje_mjesto')->id('imanje_mjesto') !!}
													
													</div>
													<div class="col-lg-3">
														{!! BootForm::text('imanje_pbroj', 'imanje_pbroj')->id('imanje_pbroj') !!}
													
													</div>
													
							
											</div>


											<div class="col-lg-12">
												<hr>
											</div>
											

											<div class="col-lg-12 bg-blue-hoki">
												<div class="col-lg-3">
													{!! BootForm::text('broj_zivotinje', 'broj_zivotinje')->class("form-control select2me")->id('zivotinja');  !!}
												</div>

												<div class="col-lg-3">
													{!! BootForm::text('spol_zivotinje', 'spol_zivotinje');  !!}
												</div>

												<div class="col-lg-3">
													{!! BootForm::text('Vrsta Zivotinja', 'imanje_vrsta_zivotinje') !!}
												</div>

												<div class="col-lg-3">
													{!! BootForm::text('Ukupan br.', 'imanje_ukupan_broj_zivotinja') !!}
												</div>

												<div class="col-lg-3">
													{!! BootForm::text('datum_pregled', 'datum_pregled') !!}
												</div>
												<div class="col-lg-3">
													{!! BootForm::select('tip_pregled', 'tip_pregled')->options(
																								[
																									
																									'vakcinacija' => 'vakcinacija', 
																									'pregled' => 'pregled', 
																							
																								])->class("form-control select2me"); !!}
												</div>
												<div class="col-lg-3">
													{!! BootForm::text('naziv_pregled', 'naziv_pregled')->class("form-control select2me pregled");  !!}
												</div>
											</div>

											<div class="col-lg-12">
												<hr>
											</div>

											<div class="col-lg-12 bg-blue-steel">
												<div class="col-lg-3">
													{!! BootForm::select('rezultat_pregled', 'rezultat_pregled')->options(
																								[
																									'' => ' --- ', 
																									'poz.(+)' => 'poz.(+)', 
																									'neg.(-)' => 'neg.(-)', 
																							
																								])->select('')->class("form-control select2me"); !!}
												</div>	
												<div class="col-lg-3">
													{!! BootForm::text('broj_nalaza', 'broj_nalaza') !!}
												</div>
												<div class="col-lg-3">
													{!! BootForm::text('datum_nalaza', 'datum_nalaza') !!}
												</div>
												<div class="col-lg-3">
													{!! BootForm::text('naziv_oznaka_vakcine', 'naziv_oznaka_vakcine') !!}
												</div>	
												
											</div>



												
											<div class="col-lg-12">
												{!! BootForm::textArea('komentar', 'komentar')->rows(3) !!}
												{{-- //Logged user id --}}
												{!! BootForm::hidden('user_id')->value($authUser->id) !!}
												
												{{-- //Record lock? --}}
												{!! BootForm::submit('Spasi')->class('btn blue') !!}

											</div>	

											{!! BootForm::close() !!}

								</div>
								</div>

							</div>
								
						</div>

									{{ show_dump($pregled) }}
							

							</div>
						</div>
					</div>







</div>


@stop


