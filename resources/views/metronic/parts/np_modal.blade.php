<div id="responsive" class="modal fade" tabindex="-1" data-width="760">
	<div class="modal-header">
		<button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
		{{-- <h4 class="modal-title">Responsive</h4> --}}<h4>Dodaj novi Pregled</h4>
	</div>
	<div class="modal-body">
		<div class="row">
			<div class="col-md-12">
{{-- 			<div class="alert alert-danger">
				<strong>Info!</strong> Sekcija nije funkcionalna.
			</div> --}}
				
								<div class="row">

									
									
										{!! BootForm::open()->action('/pregledi/create')->method('POST') !!}
											<div class="col-lg-12 bg-blue">
													<div class="col-lg-3">
														{!! BootForm::select('kanton', 'kanton')->id('kanton')->class("form-control select2me")->options($kantoni)!!}
													</div>
													<div class="col-lg-3">
														{{-- {{$authUser->stanica}} --}}
														
														{!! BootForm::select('imanje_id_broj', 'imanje_id_broj')->id('imanje')->class("form-control select2me")->options(
																												[
																												'---'=>'---',
																												'1'=>'1',
																												'2'=>'2',
																												])!!}

														{{-- {!! BootForm::text('stanica_id', 'stanica_id')->value($authUser->stanica->stanica_id) !!} --}}
														{!! BootForm::hidden('stanica_id')->value($authUser->stanica->id) !!}
														{{-- {{dump($authUser->id)}} --}}
														{{-- {!! BootForm::text('korisnik_id', 'korisnik_id') !!} --}}
													</div>

													<div class="col-lg-6">
														{!! BootForm::text('imanje_ime', 'imanje_ime')->id('imanje_ime1') !!}
														
													</div>

													<div class="col-lg-6">
														{!! BootForm::text('imanje_adresa', 'imanje_adresa')->id('imanje_adresa') !!}
													</div>

													<div class="col-lg-3">
														{!! BootForm::text('imanje_mjesto', 'imanje_mjesto')->id('imanje_mjesto') !!}
													
													</div>
													<div class="col-lg-3">
														{!! BootForm::text('imanje_pbroj', 'imanje_pbroj')->id('imanje_pbroj') !!}
													
													</div>
													
							
											</div>


											<div class="col-lg-12">
												<hr>
											</div>
											

											<div class="col-lg-12 bg-blue-hoki">
												<div class="col-lg-3">
													{!! BootForm::select('broj_zivotinje', 'broj_zivotinje[]')->class("form-control")->id('zivotinja')->multiple('multiple');  !!}
												</div>

{{-- 												<div class="col-lg-3">
													{!! BootForm::text('spol_zivotinje', 'spol_zivotinje');  !!}
												</div> --}}
{{-- 
												<div class="col-lg-3">
													{!! BootForm::text('Vrsta Zivotinja', 'imanje_vrsta_zivotinje') !!}
												</div> --}}
{{-- 
												<div class="col-lg-3">
													{!! BootForm::text('Ukupan br.', 'imanje_ukupan_broj_zivotinja') !!}
												</div> --}}

												<div class="col-lg-3">
													{!! BootForm::text('datum_pregled', 'datum_pregled') !!}
												</div>
												<div class="col-lg-3">
													{!! BootForm::select('tip_pregled', 'tip_pregled')->options(
																								[
																									
																									'vakcinacija' => 'vakcinacija', 
																									'pregled' => 'pregled', 
																							
																								])->class("form-control select2me"); !!}
												</div>
												<div class="col-lg-3">
													{!! BootForm::text('naziv_pregled', 'naziv_pregled')->class("form-control pregled");  !!}
												</div>
											</div>

											<div class="col-lg-12">
												<hr>
											</div>

											<div class="col-lg-12 bg-blue-steel">
												<div class="col-lg-3">
													{!! BootForm::select('rezultat_pregled', 'rezultat_pregled')->options(
																								[
																									'' => ' --- ', 
																									'poz.(+)' => 'poz.(+)', 
																									'neg.(-)' => 'neg.(-)', 
																							
																								])->select('')->class("form-control select2me"); !!}
												</div>	
												<div class="col-lg-3">
													{!! BootForm::text('broj_nalaza', 'broj_nalaza') !!}
												</div>
												<div class="col-lg-3">
													{!! BootForm::text('datum_nalaza', 'datum_nalaza') !!}
												</div>
												<div class="col-lg-3">
													{!! BootForm::text('naziv_oznaka_vakcine', 'naziv_oznaka_vakcine') !!}
												</div>	
												
											</div>



												
											<div class="col-lg-12">
												{!! BootForm::textArea('komentar', 'komentar')->rows(3) !!}
												{{-- //Logged user id --}}
												{!! BootForm::hidden('user_id')->value($authUser->id) !!}
												
												{{-- //Record lock? --}}
												{!! BootForm::submit('Spasi')->class('btn blue') !!}

											</div>	

																{{-- //Logged user id --}}
											{{-- 	{!! BootForm::hidden('user_id')->value(getuser()->id) !!}
												{!! BootForm::hidden('stanica_id')->value(getuser()->stanica_id) !!} --}}
				
				{{-- //Record lock? --}}

												{!! BootForm::close() !!}

									</div>


				
			</div>

		</div>
	</div>
{{-- 	<div class="modal-footer">
		<button type="button" data-dismiss="modal" class="btn btn-default">Close</button>
		<button type="button" class="btn blue">Save changes</button>
	</div> --}}
</div>
<!-- END responsive