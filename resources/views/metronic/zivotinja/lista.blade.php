@extends ('metronic')

@section('page_level')
  <link rel="stylesheet" type="text/css" href="{{ url('metronic') }}/assets/global/plugins/select2/select2.css"/>
  <link rel="stylesheet" type="text/css" href="{{ url('metronic') }}/assets/global/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.css"/>
  <link href="{{ url('metronic') }}/assets/global/plugins/bootstrap-modal/css/bootstrap-modal-bs3patch.css" rel="stylesheet" type="text/css"/>
  <link href="{{ url('metronic') }}/assets/global/plugins/bootstrap-modal/css/bootstrap-modal.css" rel="stylesheet" type="text/css"/>

@stop

@section('page_plugins')
  <script type="text/javascript" src="{{ url('metronic') }}/assets/global/plugins/select2/select2.min.js"></script>
  <script type="text/javascript" src="{{ url('metronic') }}/assets/global/plugins/datatables/media/js/jquery.dataTables.min.js"></script>
  <script type="text/javascript" src="{{ url('metronic') }}/assets/global/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.js"></script>
  <script src="{{ url('metronic') }}/assets/admin/pages/scripts/table-managed.js"></script>

  <script src="{{ url('metronic') }}/assets/global/plugins/bootstrap-modal/js/bootstrap-modalmanager.js" type="text/javascript"></script>
  <script src="{{ url('metronic') }}/assets/global/plugins/bootstrap-modal/js/bootstrap-modal.js" type="text/javascript"></script>


    <script>
        jQuery(document).ready(function() {
        //    Metronic.init(); // init metronic core components
        // Layout.init(); // init current layout
        // Demo.init(); // init demo features
           TableManaged.init();
        });

    </script>

@stop

@section('content')



<div class="page-content-wrapper">
    <div class="page-content">
      <!-- BEGIN PAGE HEAD -->
      <div class="page-head">

{{--  BEGIN PAGE TITLE
  <div class="page-title">
    <h1>Dashboard <small>statistics & reports</small></h1>
  </div>
  END PAGE TITLE

</div> --}}
      <!-- END PAGE HEAD -->
      <!-- BEGIN PAGE BREADCRUMB -->

      <!-- END PAGE BREADCRUMB -->
      <!-- BEGIN PAGE CONTENT INNER -->

      <div class="row">
        <div class="col-md-12">





          <div class="portlet box green tabbable">
                      <div class="portlet-title">
                        <div class="caption">
                          <i class="fa fa-gift"></i>Imanja

{{--                           <a class="btn blue" data-toggle="modal" href="#responsive">
                                  Novi korisnik </a> --}}

                        </div>


                        <ul class="nav nav-tabs">

                          <li>
                            <a href="#portlet_tab3" data-toggle="tab">
                            Tab 3 </a>
                          </li>
                      {{--    <li>
                            <a href="#portlet_tab2" data-toggle="tab">
                            Tab 2 </a>
                          </li>
                          <li class="active">
                            <a href="#portlet_tab1" data-toggle="tab">
                            Tab 1 </a>
                          </li> --}}
                        </ul>
                      </div>





                      <div class="portlet-body">
                        <div class="tab-content">
                          <div class="tab-pane active" id="portlet_tab1">
                              <div class="col-lg-12">

                                    <div class="col-lg-3">

                                        @if (Request::is('zivotinja') == true)
                                          {!! BootForm::open()->action('/zivotinja/create')->method('POST') !!}
                                        @else
                                          {{-- {!! BootForm::open()->action('/zivotinja/create')->method('POST') !!} --}}
                                          {!! BootForm::open()->action("/zivotinja/$zivotinja->id/edit", $zivotinja)->put() !!}
                                          {!! BootForm::bind($zivotinja) !!}
                                        @endif
                                               {{-- {!! BootForm::text('pg_broj', 'pg_broj') !!} --}}

                                               {!! BootForm::text('broj_zivotinje', 'broj_zivotinje') !!}

                                    </div>

                                    <div class="col-lg-2">
                                           {!! BootForm::text('vrsta_zivotinje', 'vrsta_zivotinje') !!}

                                    </div>

                                    <div class="col-lg-1">
                                           {!! BootForm::select('spol_zivotinje', 'spol_zivotinje')
                                                                          ->options([
                                                                            'M'=>'M',
                                                                            'Z'=>'Z' ]) !!}

                                    </div>

                                    <div class="col-lg-2">
                                           {!! BootForm::select('status_zivotinje', 'status_zivotinje')->options([
                                                                            'A'=>'A',
                                                                            'N'=>'N' ]) !!}


                                    </div>

                                    <div class="col-lg-3">
                                           {!! BootForm::select('imanje_id', 'imanje_id')->options($imanja)->class("form-control select2me"); !!}

                                    </div>


                                    <div class="col-lg-12 text-right">
                                          {!! BootForm::submit('save')->class('btn blue') !!}
                                           {!! BootForm::close() !!}
                                    </div>


                            </div>

                            <div class="col-lg-12">
                               <hr>
                            </div>


                              Stranica:{{$data->currentPage()}} od {{$data->lastPage()}} <br> {{$data->links()}}

                            <table class="table table-striped table-bordered table-hover" id="sample_1">
                                          <thead>
                                          <tr>
                                            <th class="table-checkbox">
                                              <input type="checkbox" class="group-checkable" data-set="#sample_1 .checkboxes"/>
                                            </th>
                                            <th>
                                               Broj goveda
                                            </th>
                                            <th>
                                               Spol
                                            </th>
                                            <th>
                                               Imanje
                                            </th>
                                            <th>
                                               Vrsta
                                            </th>
                                            <th>
                                                Status
                                            </th>
                                            <th>
                                               Akcije
                                            </th>

                                          </tr>
                                          </thead>
                                          <tbody>
                                            @if (isset($data))
                                            @foreach ($data as $q)
                                            {{-- {{dump($q)}} --}}

                                              <tr class="odd gradeX">
                                                <td>
                                                  <input type="checkbox" class="checkboxes" value="1"/>
                                                </td>
                                                <td>
                                                  {!! link_to("zivotinja/$q->id", $q->broj_zivotinje) !!}

                                               </td>
                                                <td>
                                                   {!! $q->spol_zivotinje !!}

                                                </td>
                                                <td>
                                                   {!! ($q->imanje->id_broj) !!}

                                                </td>
                                                <td>
                                                   {!! ($q->vrsta_zivotinje) !!},

                                                </td>
                                                <td>
                                                   {!! ($q->status_zivotinje) !!}


                                                </td>
                                                <td>
                                                   <a href="/zivotinja/{{$q->id}}/d">

                                                        <button class="btn red-haze btn-sm" type="button">
                                                                        <i class="fa fa-close"></i>
                                                                        </button>
                                                   </a>
                                                </td>

                                              </tr>
                                            @endforeach

                                            @endif

                                          </tbody>
                                        </table>


                                        @if (count($errors) > 0)
                                            <div class="alert alert-danger">
                                                <ul>
                                                    @foreach ($errors->all() as $error)
                                                        <li>{{ $error }}</li>
                                                    @endforeach
                                                </ul>
                                            </div>
                                        @endif
                          </div>
{{--                          <div class="tab-pane" id="portlet_tab2">
                            <p>
                               Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo.
                            </p>
                          </div>
                          <div class="tab-pane" id="portlet_tab3">
                            <p>
                               Ut wisi enim ad btn-smm veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat. Duis autem vel eum iriure dolor in hendrerit in vulputate velit esse molestie consequat, vel illum dolore eu feugiat nulla facilisis at vero eros et accumsan et iusto odio dignissim qui blandit praesent luptatum zzril delenit augue duis dolore te feugait nulla facilisi.
                            </p>
                          </div> --}}
                        </div>
                      </div>
                    </div>

          {{-- {{show_dump($data)}} --}}


        </div>
      </div>


      <!-- END PAGE CONTENT INNER -->


    </div>
  </div>

@stop

<script>

</script>
