	<div class="page-sidebar-wrapper">
		<!-- DOC: Set data-auto-scroll="false" to disable the sidebar from auto scrolling/focusing -->
		<!-- DOC: Change data-auto-speed="200" to adjust the sub menu slide up/down speed -->
		<div class="page-sidebar navbar-collapse collapse">
			@include('flash::message')
			<!-- BEGIN SIDEBAR MENU -->
			<!-- DOC: Apply "page-sidebar-menu-light" class right after "page-sidebar-menu" to enable light sidebar menu style(without borders) -->
			<!-- DOC: Apply "page-sidebar-menu-hover-submenu" class right after "page-sidebar-menu" to enable hoverable(hover vs accordion) sub menu mode -->
			<!-- DOC: Apply "page-sidebar-menu-closed" class right after "page-sidebar-menu" to collapse("page-sidebar-closed" class must be applied to the body element) the sidebar sub menu mode -->
			<!-- DOC: Set data-auto-scroll="false" to disable the sidebar from auto scrolling/focusing -->
			<!-- DOC: Set data-keep-expand="true" to keep the submenues expanded -->
			<!-- DOC: Set data-auto-speed="200" to adjust the sub menu slide up/down speed -->
			<ul class="page-sidebar-menu " data-keep-expanded="false" data-auto-scroll="true" data-slide-speed="200">

			<li>

				<a href="{{url('metronic/templates/admin4/')}}" target="_blank">
				<i class="icon-settings"></i>
				<span class="title">Theme</span>
				</a>

			<hr>
			</li>
				<li class="start active ">
					<a href="{{url('/')}}">
					<i class="icon-home"></i>
					<span class="title">Dashboard</span>
					</a>
				</li>
				<li class="start active ">
					<a href="{{url('stanice')}}">
					<i class="fa fa-building"></i>
					<span class="title">Stanice</span>
					</a>
				</li>

				@if (getuser()->isSuperAdmin())
				<li class="start active ">
					<a href="{{url('korisnici')}}">
					<i class="fa fa-users"></i>
					<span class="title">Korisnici</span>
					</a>
				</li>
				@endif

				<li class="start active ">
					<a href="{{url('imanja')}}">
					<i class="fa fa-home"></i>
					<span class="title">Imanja</span>
					</a>
				</li>
{{-- 				<li class="start active ">
					<a href="{{url('zivotinja')}}">
					<i class="icon-home"></i>
					<span class="title">Zivotinje</span>
					</a>
				</li> --}}

				<li class="start active ">
					<a href="{{url('pregledi')}}">
					<i class="fa fa-archive"></i>
					<span class="title">Pregledi</span>
					</a>
				</li>

{{-- 				<li class="start active ">
					<a href="{{url('mame')}}">
					<i class="icon-home"></i>
					<span class="title">Mame</span>
					</a>
				</li>

				<li class="start active ">
					<a href="{{url('tasks')}}">
					<i class="icon-home"></i>
					<span class="title">Tasks</span>
					</a>
				</li> --}}



			</ul>
			<!-- END SIDEBAR MENU -->
		</div>
	</div>
