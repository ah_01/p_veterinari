@extends ('metronic')

@section('page_level')
  <link rel="stylesheet" type="text/css" href="{{ url('metronic') }}/assets/global/plugins/select2/select2.css"/>
  <link rel="stylesheet" type="text/css" href="{{ url('metronic') }}/assets/global/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.css"/>
  <link href="{{ url('metronic') }}/assets/global/plugins/bootstrap-modal/css/bootstrap-modal-bs3patch.css" rel="stylesheet" type="text/css"/>
  <link href="{{ url('metronic') }}/assets/global/plugins/bootstrap-modal/css/bootstrap-modal.css" rel="stylesheet" type="text/css"/>

@stop

@section('page_plugins')
  <script type="text/javascript" src="{{ url('metronic') }}/assets/global/plugins/select2/select2.min.js"></script>
  <script type="text/javascript" src="{{ url('metronic') }}/assets/global/plugins/datatables/media/js/jquery.dataTables.min.js"></script>
  <script type="text/javascript" src="{{ url('metronic') }}/assets/global/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.js"></script>
  <script src="{{ url('metronic') }}/assets/admin/pages/scripts/table-managed.js"></script>

  <script src="{{ url('metronic') }}/assets/global/plugins/bootstrap-modal/js/bootstrap-modalmanager.js" type="text/javascript"></script>
  <script src="{{ url('metronic') }}/assets/global/plugins/bootstrap-modal/js/bootstrap-modal.js" type="text/javascript"></script>


    <script>
        jQuery(document).ready(function() {

        //    Metronic.init(); // init metronic core components
        // Layout.init(); // init current layout
        // Demo.init(); // init demo features
           TableManaged.init();
        });

    </script>

@stop

@section('content')

<?php

  if (isset($imanje)) {
    $imanje = $imanje->first();
  }
 ?>

<div class="page-content-wrapper">
    <div class="page-content">
      <!-- BEGIN PAGE HEAD -->
      <div class="page-head">

      </div>


      <!-- END PAGE HEAD -->
      <!-- BEGIN PAGE BREADCRUMB -->

      <!-- END PAGE BREADCRUMB -->
      <!-- BEGIN PAGE CONTENT INNER -->

      <div class="row">
        <div class="col-md-12">


          <div class="portlet box green">
                      <div class="portlet-title">
                        <div class="caption">
                          <i class="fa fa-gift"></i>Imanja @if (isset($imanje)) - {{$imanje->id_broj}}  @endif
                        </div>


                        <ul class="nav nav-tabs">

                          <li>
                          {{-- <a href="#portlet_tab3" data-toggle="tab">
                            Tab 3 </a>
                          </li> --}}

                        </ul>
                      </div>


                <div class="portlet-body">

                  <div class="tab-content">
                    <div class="tab-pane active" id="portlet_tab1">


                      {{-- START: PRETRAGA  --}}
                      @unless (Request::is('imanja/*'))


                          {{-- ------------------------------------------- START: Trazi imanja ---------------------------------------------------  --}}
                           <div class="alert alert-info">

                               <div class="col-lg-2">

                               @if (Request::is('imanja') == true)
                                 {!! BootForm::open()->action('/imanja/pretraga')->method('POST') !!}
                               @endif

                                      {!! BootForm::text('id_broj', 'id_broj')->class('form-control input-sm') !!}

                               </div>
                               <div class="col-lg-3">
                                      {!! BootForm::text('ime', 'ime')->class('form-control input-sm') !!}

                               </div>

                               <div class="col-lg-3">
                                      {!! BootForm::text('mjesto', 'mjesto')->class('form-control input-sm') !!}

                               </div>

                               <div class="col-lg-3">
                                      {!! BootForm::text('pbroj', 'pbroj')->class('form-control input-sm') !!}
                               </div>



                               <div class="col-lg-1 text-right">
                                     {!! BootForm::submit('Trazi')->class('btn blue btn-sm') !!}
                                      {!! BootForm::close() !!}
                               </div>
                             <div class=""> <br><br><br> </div>
                             </div>
                          {{-- ------------------------------------------- START: Trazi imanja ---------------------------------------------------  --}}

                      @endunless
                      {{-- END: PRETRAGA  --}}


                         {{-- Prikazati dormu imanja u bilo kojem slucaju jer ako nema varijable $imanje forma se koristi za kreiranje ako  ima varijable forma se koristi za editovanje imanaja --}}

                       @if (Request::is('imanja*') == true || Request::is('imanja/pretraga') == true)

                          @include('metronic.imanje.imanje_new')
                       @endif


                         {{-- Ako je imanje definisano  prikazi zivotinje na imanju  - dio zivotinje_lista --}}
                         @if (isset($imanje))
                           Mjere izvrsene na imanju (po godinama): <br>

                           @foreach($imanje->preglediGroupDateBy('year') as $key => $value)

                             <a href="{{ url('pregledi/godina/' . $key, $imanje->id) }}" class="blue"> {{ $key }} </a> : {{ count($value->groupBy("naziv_pregled")) }} mjera izvrseno.

                           @endforeach
                           <hr>
                           @include('metronic.imanje.zivotinje_lista')
                         @endif





                          <div class="col-lg-12">
                               <hr>

                          </div>

                          @if (!isset($imanje))
                            @include('metronic.imanje.imanja_lista')
                          @endif
                        </div>
                      </div>
                    </div>

          {{-- {{show_dump($data)}} --}}


        </div>
       </div>


       <!-- END PAGE CONTENT INNER -->



  </div>
  </div>

@stop

<script>

</script>
