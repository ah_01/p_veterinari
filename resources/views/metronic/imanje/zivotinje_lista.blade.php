<div class="co-lg-12">
  <div class="panel panel-success">
      <div class="panel-heading">
        <div class="panel-title">Lista zivotinja na imanju</div>
      </div>

      <div class="panel-body">




<div class="portlet"><a href="{{url('pregledi/create/'. $imanje->id)}}" class="btn blue btn-xs">Novi pregled</a></div>
@include('metronic.imanje.zivotinja_new')

@if (count($imanje->zivotinje)>0)
  {{-- <h5>Zivotinje na imanju(aktivne):  {{count($imanje->zivotinje->first()->aktivneZivotinjeNum())}} </h5> AHA this function gets all active animals --}}
  {{-- {{dump($imanje)}} --}}
  <h5>Zivotinje na imanju(aktivne):  {{count($imanje->zivotinje->where('status_zivotinje', 'A'))}},  od ukupno zavedenih {{count($imanje->zivotinje)}}  </h5>
@endif
<div class="col-lg-12 portlet">
                <table class="table table-bordered table-striped" style="font-size:13px;">


                <thead>
                <tr>
                  <th>
                     #
                  </th>
                  <th>
                     Broj Zivotinje
                  </th>
                  <th class="hidden-480">
                     Spol zivotinje
                  </th>
                  <th class="hidden-480">
                     Vrsta zivotinje
                  </th>
                  <th>
                     Status zivotinje
                  </th>

                </tr>
                </thead>


                {{-- */ $i = 1; /*  --}}
                <tbody>
              @foreach ($imanje->zivotinje->sortBy('status_zivotinje') as $zivotinja)


                <tr>
                  <td>
                  {{ $i }}
                  {{-- */ ++$i; /* --}}
                  </td>
                  <td>
                  {{-- {{dump(Request::url())}} --}}
                     {!! link_to(action('ImanjeController@index', ['id'=>Request::segment(2), 'gid'=>$zivotinja->id]), $zivotinja->broj_zivotinje) !!}
                  </td>
                  <td class="hidden-480">
                     {{$zivotinja->spol_zivotinje}}
                  </td>
                  <td class="hidden-480">
                     {{$zivotinja->vrsta_zivotinje}}
                  </td>
                  <td>
                     @if ($zivotinja->status_zivotinje == 'A')
                          <div class="label label-success">Aktivna</div>
                     @endif

                     @if ($zivotinja->status_zivotinje == 'N')
                          <div class="label label-danger">Neaktivna</div>
                     @endif
                  </td>


                </tr>

              @endforeach


                </tbody>
                </table>
              </div>
            </div>
      </div>
