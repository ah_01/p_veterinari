<div class="alert alert-info">
  <strong>Informacija:</strong>
  <br>Donja tabela prikazuje {{$data->count()}} nedavno izmjnjenih/pretrazivanih unosa iz tabele imanja. Podatci unutar te stranice (prikazanih  {{$data->count()}} unosa) se mogu aktivno pretraziti sa poljem "Search" iznad tabele, za ostale unose koristitie pretragu iznad.

</div>
  Stranica:{{$data->currentPage()}} od {{$data->lastPage()}} <br> {{$data->links()}}


<table class="table table-striped table-bordered table-hover" id="sample_1">
                <thead>
                <tr>

                  <th>
                     Broj imanja
                  </th>

                  <th>
                     Broj Zivotinja
                  </th>

                  <th>
                     Ime
                  </th>

                  <th>
                     Adresa
                  </th>

                  <th>
                     Akcije
                  </th>

                </tr>
                </thead>
                <tbody>
                  @if (isset($data))
                  @foreach ($data as $q)
                  {{-- {{dump($q)}} --}}

                    <tr class="odd gradeX">

                      <td>

                          @if (!empty($q->id_broj))
                            {!! link_to("imanja/$q->id", $q->id_broj) !!}
                          @elseif (!empty($q->jmbg))
                            {!! link_to("imanja/$q->id", $q->jmbg) !!}
                          @endif

                      </td>
                      <td>
                         {!! count($q->zivotinje) !!}

                      </td>
                      </td>
                      <td>
                         {!! $q->ime !!}

                      </td>
                      <td>
                         {!! ($q->adresa) !!},
                         {!! ($q->mjesto) !!},
                         {!! ($q->pbroj) !!}

                      </td>

                      <td>
                         <a href="/imanja/{{$q->id}}/d">

                              <button class="btn red-haze btn-sm" type="button">
                                              <i class="fa fa-close"></i>
                                              </button>

                         </a>
                      </td>

                    </tr>
                  @endforeach

                  @endif

                </tbody>
        </table>


              @if (count($errors) > 0)
                  <div class="alert alert-danger">
                      <ul>
                          @foreach ($errors->all() as $error)
                              <li>{{ $error }}</li>
                          @endforeach
                      </ul>
                  </div>
              @endif
</div>
