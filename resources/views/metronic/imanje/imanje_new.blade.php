<div class="co-lg-12">
  <div class="panel panel-success">
      <div class="panel-heading">
        <div class="panel-title">Dodaj novo imanje</div>
      </div>

      <div class="panel-body">

          <div class="col-lg-4">

          @if (isset($imanje) == false)
            {!! BootForm::open()->action('/imanja/create')->method('POST') !!}
          @else
            {!! BootForm::open()->action("/imanja/$imanje->id/edit", $imanje)->put() !!}
            {!! BootForm::bind($imanje) !!}
          @endif

                 {{-- {!! BootForm::text('pg_broj', 'pg_broj') !!} --}}

                 {!! BootForm::text('id_broj', 'id_broj')->class('form-control input-sm') !!}

          </div>
          <div class="col-lg-4">
                 {!! BootForm::text('ime', 'ime')->class('form-control input-sm') !!}

          </div>
          <div class="col-lg-4">
                 {!! BootForm::text('jmbg', 'jmbg')->class('form-control input-sm') !!}

          </div>



          <div class="col-lg-4">
                 {!! BootForm::text('adresa', 'adresa')->class('form-control input-sm') !!}

          </div>

          <div class="col-lg-4">
                 {!! BootForm::text('mjesto', 'mjesto')->class('form-control input-sm') !!}

          </div>

          <div class="col-lg-4">
                 {!! BootForm::text('pbroj', 'pbroj')->class('form-control input-sm') !!}
          </div>



          <div class="col-lg-12 text-right">
                {!! BootForm::submit('save')->class('btn blue btn-sm') !!}
                 {!! BootForm::close() !!}
          </div>

      </div>
  </div>
</div>
