<div class="panel panel-success">
                  <div class="panel-heading">
                    <h3 class="panel-title">Dodaj zivotinju na imanje</h3>
                  </div>
                  <div class="panel-body">
                          <div class="col-lg-12">
                          <!-- {{ dump(isset($zivotinja)) }} -->
                          <div class="col-lg-3">

                              @if (isset($zivotinja) == false)
                                {!! BootForm::open()->action('/zivotinja/create')->method('POST') !!}
                              @else
                                {{-- {!! BootForm::open()->action('/zivotinja/create')->method('POST') !!} --}}
                                {!! BootForm::open()->action("/zivotinja/$zivotinja->id/edit", $zivotinja)->put() !!}
                                {!! BootForm::bind($zivotinja) !!}
                              @endif
                                     {{-- {!! BootForm::text('pg_broj', 'pg_broj') !!} --}}

                                     {!! BootForm::text('broj_zivotinje', 'broj_zivotinje')->class('form-control input-sm') !!}


                          </div>

                          <div class="col-lg-2">
                                 {!! BootForm::text('vrsta_zivotinje', 'vrsta_zivotinje')->class('form-control input-sm') !!}

                          </div>

                          <div class="col-lg-1">
                                 {!! BootForm::select('spol', 'spol_zivotinje')
                                                                ->options([
                                                                  'M'=>'M',
                                                                  'Z'=>'Z' ])->class('form-control input-sm') !!}

                          </div>

                          <div class="col-lg-1">
                                 {!! BootForm::select('status', 'status_zivotinje')->options([
                                                                  'A'=>'A',
                                                                  'N'=>'N' ])->class('form-control input-sm') !!}


                          </div>

                        {!! BootForm::hidden('imanje_id')->value($imanje->id) !!}
                          <div class="col-lg-2 text-center">
                            <br>

                                {!! BootForm::submit('save')->class('btn blue btn-sm') !!}
                                 {!! BootForm::close() !!}
                          </div>

                        </div>

                  </div>
                </div>
