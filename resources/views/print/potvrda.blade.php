<html>
<head>
  <meta charset="UTF-8">
  <title>Document</title>

  <link href="{{ url('metronic') }}/assets/global/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css"/>
  <link href="{{ url('metronic') }}/assets/global/plugins/bootstrap-switch/css/bootstrap-switch.min.css" rel="stylesheet" type="text/css"/>
  <link href='https://fonts.googleapis.com/css?family=Ubuntu:400,400italic,500,500italic,700,700italic,300italic,300' rel='stylesheet' type='text/css'>
  <style>
        body {
          width: 100%;
          height: 100%;
          /*margin: 5mm;*/
          padding: 0mm;
          /*background-color: #CCCCCC;*/
          font: 9pt "Ubuntu";
      }
      * {
          box-sizing: border-box;
          -moz-box-sizing: border-box;
      }
      .page {
          width: 205mm;
          min-height: 285mm;
          padding: 4mm;
          margin: 5mm auto;
          border: 0px red solid;
          /*border-radius: 5px;*/
          background: white;
          /*box-shadow: 0 0 5px rgba(0, 0, 0, 0.1);*/
      }
  /*     .subpage {
      padding: 1cm;
      border: 5px red solid;
      height: 257mm;
      outline: 2cm #FFEAEA solid;
  }
   */
      .div_7_3{
          padding: 1mm;
          border: 1px gray solid;
          height: 36mm;
          width: 70mm;
          font-size: 9px;
          margin:.5mm auto;
          /*float:left;*/

          /*outline: 2cm #FFEAEA solid;*/
      }
      .div_border{
        padding: 0mm;
          border: 1px gray solid;
          min-height: 9mm;
  /*        height: 36mm;
          width: 70mm;
          font-size: 9px;
          margin:.5mm auto;*/
          /*float:left;*/

          /*outline: 2cm #FFEAEA solid;*/
          padding:1px;
      }

      .header{
          padding-left: 10mm;
          padding-right: 10mm;
          margin:1mm auto;
      }
      .tabela{
         margin: 11mm 0mm 11mm 0mm;
      }
      .pregledi{
         border: 0px green solid;
         min-height: 13cm;
      }

      .cell{
          min-height: 13mm;
          font: 6pt "Tahoma";
          padding:3px;
      }

      .vrijednost{
           vertical-align: bottom;
           font-size: 9;
           font-weight: 600;
           /*margin-top: 3mm;*/
      }
      .info{
        font-size: 5pt;

      }
    }

      @page {
          size: A4;
  /*        margin: 10mm;
          padding:10mm;*/
          page-break-after: always;

      }
      @print {
          size: A4;
          margin: 0;
      }


  </style>

</head>
<body>




  <?php
  date_default_timezone_set('UTC');
    // $slice = $r->splice(0,24);
    // dd($data);

$datag = $data->groupBy('tip_pregled');
    // dump($data);
//differentiate vakcinaacija and pregled
$vakcinacija = $datag->get('vakcinacija');
$pregled = $datag->get('pregled');
$stoka = $data->groupBy('broj_zivotinje');

// dd($vakcinacija);

//Group by naziv
$vakcinacija_grouped = $vakcinacija->groupBy('naziv_pregled');
$pregled_grouped = $pregled->groupBy('naziv_pregled');
// dump($pregled_grouped);



// dump("Grouped <br>");
// dump($vakcinacija_grouped );

// $vakcinacija = $vakcinacija->groupBy('naziv_pregled');
$vakcinacija = $vakcinacija_grouped->keys();
$pregled = $pregled_grouped->keys();
    // dump($pregled);

   ?>

<div class="page">
           {{-- <div class="subpage">Page 1/2</div>     --}}



        <div class="row">

            <div class="col-xs-12 ">
              <div class="col-xs-1">
                Ustanova:   <br>
                Adresa:   <br>
                Telefon:   <br>
                Email:   <br>
              </div>
              <div class="col-xs-11">
                  <b>{{$authUser->stanica->stanica}}</b><br>
                  {{$authUser->stanica->adresa}}, {{$authUser->stanica->pbroj}} {{$authUser->stanica->mjesto}} <br>
                  {{$authUser->stanica->telefon}} <br>
                  {{$authUser->stanica->email}} <br>

              </div>


            </div>


            <div class="col-xs-12 header text-center">
               <h4>Potvrda o provedenim mjerama</h4>
               <div  class="text-justify">
               U skladu sa odredbama o mjerama kontrole zaraznih bolesti zivotinja i njihovom provodjenju  u 2015. godini, izdaje se potvrda za imanje vlasnika
               <b>{{$datag->first()->first()->imanje_ime}}</b>, broj imanja
               <b>{{$datag->first()->first()->imanje_id_broj}}</b>, adresa
               <b>{{$datag->first()->first()->imanje_adresa}}</b>, mjesto
               <b>{{$datag->first()->first()->imanje_mjesto}}</b>, na kojem su provedena sljedeca ispitivanja/vakcinacije:
               </div>

            </div>
        </div>

        <div class="col-xs-12 tabela pregledi">

        @foreach ($vakcinacija as $vakcina)
{{--               {{dump($vakcina)}}
              {{dump($vakcinacija_grouped->get($vakcina))}}
              {{dump($vakcinacija_grouped->get($vakcina)->count())}} --}}
              {{-- ROW: vakcina START --}}
                  <div class="row">

                      <div class="col-lg-12">
                            <div class="row">

                                  <div class="col-xs-3 div_border">
                                    <div class="info">Izvrsena vakcinacija (bolest):</div>
                                    <div class="vrijednost">{{$vakcina}}</div>
                                  </div>
                                  <div class="col-xs-1 div_border">
                                    <div class="info">Datum:</div>
                                    <div class="vrijednost">{{($vakcinacija_grouped->get($vakcina)->first()->datum_pregled->toFormattedDateString())}}</div>
                                  </div>
                                  <div class="col-xs-1 div_border">
                                    <div class="info">Vrsta ziv:</div>
                                    <div class="vrijednost">{{$vakcinacija_grouped->get($vakcina)->first()->imanje_vrsta_zivotinje}}</div>
                                  </div>
                                  <div class="col-xs-1 div_border">
                                    <div class="info">Ukupan br zivotinja:</div>
                                    <div class="vrijednost">{{$vakcinacija_grouped->get($vakcina)->first()->imanje_ukupan_broj_zivotinja}}</div>
                                  </div>
                                  <div class="col-xs-2 div_border">
                                    <div class="info">Broj zivotinja na kojima je provedena mjera:</div>
                                    <div class="vrijednost">{{$vakcinacija_grouped->get($vakcina)->count()}}</div>
                                  </div>
                                  <div class="col-xs-4 div_border">
                                    <div class="info">Naziv i oznaka vakcine(serija):</div>
                                    <div class="vrijednost">{{$vakcinacija_grouped->get($vakcina)->first()->naziv_oznaka_vakcine}}</div>
                                  </div>


                              </div>


                        </div>


                  </div>

              {{-- ROW: vakcina END --}}
        @endforeach

        @foreach ($pregled as $p)
        {{-- ROW: pregled START --}}
            <div class="row">

                <div class="col-lg-12">
                      <div class="row">

                            <div class="col-xs-3 div_border">
                              <div class="info">Izvrsena vakcinacija (bolest):</div>
                              <div class="vrijednost">{{$p}}</div>
                            </div>
                            <div class="col-xs-1 div_border">
                              <div class="info">Datum:</div>
                              <div class="vrijednost">{{$pregled_grouped->get($p)->first()->datum_pregled->toFormattedDateString()}}</div>
                            </div>
                            <div class="col-xs-1 div_border">
                              <div class="info">Vrsta ziv:</div>
                              <div class="vrijednost">{{$pregled_grouped->get($p)->first()->imanje_vrsta_zivotinje}}</div>
                            </div>
                            <div class="col-xs-1 div_border">
                              <div class="info">Ukupan br zivotinja:</div>
                              <div class="vrijednost">{{$pregled_grouped->get($p)->first()->imanje_ukupan_broj_zivotinja}}</div>
                            </div>
                            <div class="col-xs-2 div_border">
                              <div class="info">Broj zivotinja na kojima je provedena mjera:</div>
                              <div class="vrijednost">{{$pregled_grouped->get($p)->count()}}</div>
                            </div>
                            <?php
                              $col = $pregled_grouped->get($p);
                              $col = $col->groupBy('rezultat_pregled');
                              // dump($col->has('poz.(+)'));
                             ?>

                            <div class="col-xs-1 div_border">
                              <div class="info">Rezultat pretrage:</div>
                              <div class="vrijednost">@if ($col->has('poz.(+)')) "poz.(+)" @else "neg.(-)" @endif</div>
                            </div>
                            <div class="col-xs-3 div_border">
                              <div class="info">Broj i datum nalaza:</div>
                              <div class="vrijednost">{{$pregled_grouped->get($p)->first()->broj_nalaza}}, {{$pregled_grouped->get($p)->first()->datum_nalaza->toFormattedDateString()}}</div>
                            </div>


                        </div>


                  </div>


            </div>

        {{-- ROW: pregled END --}}
        @endforeach

        </div>


        <div class="col-xs-12 napomene">
          U prilogu se nalazi: popis zivotinja na imanju, kopije laboratorijskih i drugih nalaza i evidencije te kopije obrazaca o provedenim mjerama tuberkulinizacije (gdje je primjenjivo).
          <br>
          Ukoliko nisu provedene mjere na odredjeni zivotinjama navesti razlog neprovodjenja mjera i idnetifikacione oznake tih zivotinja:
          <br>
          <br>
          ___________________________________________________________________________________________________________________
          <br>
          <br>
          ___________________________________________________________________________________________________________________
          <br>
          <br>
          ___________________________________________________________________________________________________________________
          <br><br>
          ___________________________________________________________________________________________________________________

        </div>


</div>

<div class="page">

<div class="row">

    <div class="col-xs-12 ">
     Popis identifikacionih oznaka na kojima su sprovedene propisane mjere i rezultati provedenih ispitivanja

    </div>

    <div class="col-xs-12 tabela pregledi">
      <div class="col-xs-12">
          <div class="col-xs-1 div_border">
            <div class="info" style="font-size: 7pt;">Rb.</div>
          </div>
          <div class="col-xs-3 div_border">
            <div class="info" style="font-size: 7pt;">Identifikacioni broj zivotinje</div>
          </div>
          <div class="col-xs-1 div_border">
            <div class="info" style="font-size: 7pt;">Spol (M/Z)</div>
          </div>
          <div class="col-xs-1 div_border">
            <div class="info" style="font-size: 7pt;">Vrsta zivotinje</div>
          </div>
          <div class="col-xs-6 div_border">
            <div class="info" style="font-size: 7pt;">Napisati naziv bolesti i rezultat ispitivanja odnosno slovo V ako je izvrsena vakcinacija</div>
          </div>
      </div>
      {{-- */$rb=0;/* --}}

      @foreach ($stoka as $govedo)
              <?php

               $pregled_col = $govedo->filter(function($item){

                    if ($item->tip_pregled == 'pregled') {
                      return $item;
                    }

               });
               $pregled_col = $pregled_col->pluck('rezultat_pregled', 'naziv_pregled');
               $p = "";
               foreach ($pregled_col as $key => $value) {
                 $p .= ($key . " " . $value . ", ");
               }


               $vakcina_col = $govedo->filter(function($item){

                    if ($item->tip_pregled == 'vakcinacija') {
                      return $item;
                    }

               });

               $vakcina_col = $vakcina_col->pluck('naziv_pregled');
               $v = "";
               foreach ($vakcina_col as $val) {
                 $v .= "V " . $val . ", ";
               }


               ?>

            {{-- ROW:START --}}
            <div class="col-xs-12">
              <div class="col-xs-1 div_border">
                <div class="info">Rb. {{++$rb}} </div>
              </div>
              <div class="col-xs-3 div_border">
                <div class="info">Identifikacioni broj zivotinje</div>
                <div class="vrijednost">{{($govedo->first()->broj_zivotinje)}}</div>
              </div>
              <div class="col-xs-1 div_border">
                <div class="info">Spol (M/Z)</div>
                <div class="vrijednost">{{$govedo->first()->spol_zivotinje}}</div>
              </div>
              <div class="col-xs-1 div_border">
                <div class="info">Vrsta zivotinje</div>
                <div class="vrijednost">{{$govedo->first()->imanje_vrsta_zivotinje}}</div>
              </div>
              <div class="col-xs-6 div_border">
                <div class="info">Napisati naziv bolesti i rezultat ispitivanja odnosno slovo V ako je izvrsena vakcinacija</div>
                <div class="vrijednost">{{$p}} {{$v}} </div>
              </div>
            </div>
            {{-- ROW:END --}}

      @endforeach



    </div>


    <div class="col-xs-12 header text-left">
      Mjesto i datum:
    </div>
    <div class="col-xs-12 header text-right">
     ......................................................................
     <br>
     Svojerucni potpis veterinara <br> (Obavezno upisati prezime stampanim slovima istaviti faksimil - pecat)
    </div>

</div>
</div>

<script src="{{ url('metronic') }}/assets/global/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
<script src="{{ url('metronic') }}/assets/global/plugins/bootstrap-switch/js/bootstrap-switch.min.js" type="text/javascript"></script>



</body>
</html>
