<link href="{{ url('metronic') }}/assets/global/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css"/>
<link href="{{ url('metronic') }}/assets/global/plugins/bootstrap-switch/css/bootstrap-switch.min.css" rel="stylesheet" type="text/css"/>
<style>
	    body {
        width: 100%;
        height: 100%;
        margin: 5mm;
        padding: 0mm;
        background-color: #CCCCCC;
        font: 10pt "Tahoma";
    }
    * {
        box-sizing: border-box;
        -moz-box-sizing: border-box;
    }
    .page {
        width: 210mm;
        min-height: 297mm;
        padding: 4mm;
        margin: 5mm auto;
        border: 1px #D3D3D3 solid;
        /*border-radius: 5px;*/
        background: white;
        /*box-shadow: 0 0 5px rgba(0, 0, 0, 0.1);*/
    }
/*     .subpage {
    padding: 1cm;
    border: 5px red solid;
    height: 257mm;
    outline: 2cm #FFEAEA solid; 
}
 */
    .div_7_3{
        padding: 1mm;
        border: 1px gray solid;
        height: 36mm;
        width: 70mm;
        font-size: 9px;
        margin:.5mm auto;
        /*float:left;*/
        
        /*outline: 2cm #FFEAEA solid;*/
    }
    .div_border{
    	padding: 0mm;
        border: 1px gray solid;
/*        height: 36mm;
        width: 70mm;
        font-size: 9px;
        margin:.5mm auto;*/
        /*float:left;*/
        
        /*outline: 2cm #FFEAEA solid;*/
    }

    .header{
        padding-left: 10mm;
        padding-right: 10mm;
        margin:1mm auto;
    }
    .tabela{
       margin: 11mm 0mm 11mm 0mm;
    }

    .cell{
        min-height: 13mm;
        font: 6pt "Tahoma";
        padding:3px;
    }
    
    .vrijednost{
         vertical-align: bottom;
         font-size: 11;
         font-weight: 600;
         /*margin-top: 3mm;*/
    }
    .info{
      font-size: 6pt;
    }
  }
    
    @page {
        size: A4;
        margin: 0;
    }

    /* Print styling */

    @media print {

    [class*="col-sm-"] {
      float: left;
    }

    [class*="col-xs-"] {
      float: left;
    }

    .col-sm-12, .col-xs-12 { 
      width:100% !important;
    }

    .col-sm-11, .col-xs-11 { 
      width:91.66666667% !important;
    }

    .col-sm-10, .col-xs-10 { 
      width:83.33333333% !important;
    }

    .col-sm-9, .col-xs-9 { 
      width:75% !important;
    }

    .col-sm-8, .col-xs-8 { 
      width:66.66666667% !important;
    }

    .col-sm-7, .col-xs-7 { 
      width:58.33333333% !important;
    }

    .col-sm-6, .col-xs-6 { 
      width:50% !important;
    }

    .col-sm-5, .col-xs-5 { 
      width:41.66666667% !important;
    }

    .col-sm-4, .col-xs-4 { 
      width:33.33333333% !important;
    }

    .col-sm-3, .col-xs-3 { 
      width:25% !important;
    }

    .col-sm-2, .col-xs-2 { 
      width:16.66666667% !important;
    }

    .col-sm-1, .col-xs-1 { 
      width:8.33333333% !important;
    }
      
    .col-sm-1,
    .col-sm-2,
    .col-sm-3,
    .col-sm-4,
    .col-sm-5,
    .col-sm-6,
    .col-sm-7,
    .col-sm-8,
    .col-sm-9,
    .col-sm-10,
    .col-sm-11,
    .col-sm-12,
    .col-xs-1,
    .col-xs-2,
    .col-xs-3,
    .col-xs-4,
    .col-xs-5,
    .col-xs-6,
    .col-xs-7,
    .col-xs-8,
    .col-xs-9,
    .col-xs-10,
    .col-xs-11,
    .col-xs-12 {
    float: left !important;
    }

    body {
      margin: 0;
      padding 0 !important;
      min-width: 768px;
    }

    .container {
      width: auto;
      min-width: 750px;
    }

    body {
      font-size: 10px;
    }

    a[href]:after {
      content: none;
    }

    .noprint, 
    div.alert, 
    header, 
    .group-media, 
    .btn, 
    .footer, 
    form, 
    #comments, 
    .nav, 
    ul.links.list-inline,
    ul.action-links {
      display:none !important;
    }

    }

</style>



	<?php 
		// $slice = $r->splice(0,24);
		
		// dump($slice);



	 ?>

	     <div class="page">
	         {{-- <div class="subpage">Page 1/2</div>     --}}
            


        <div class="row">
                <div class="col-sm-3 div_border">
        
            TESSST1 <br>
            TESSST <br>
            TESSST <br>
            TESSST <br>
      
      </div>
 

        <div class="col-sm-12 header text-center">
           <h3>Potvrda o provedenim mjerama</h3>
           <div  class="text-justify">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Cum laudantium, iure sequi, 
           quam necessitatibus consectetur dignissimos aperiam provident assumenda, perspiciatis maxime fugit nesciunt error expedita doloremque voluptatem illo deleniti exercitationem.</div>
          
        </div>
        </div>
   
              

        <div class="col-sm-12 tabela">
          <div class="row">
          
              
              <div class="col-sm-12">
                  <div class="col-sm-2 div_border cell">
                      <div class="info">Izvrsena vakcinacija (bolest):</div> <br>
                      <div class="vrijednost">ANTRAX</div> 
                      </div>
                  <div class="col-sm-1 col-sm-1 div_border cell">
                      <div class="info">Datum:</div> <br>
                      <div class="vrijednost">VAL</div> 
                  </div>
                  <div class="col-sm-1 col-sm-1  div_border cell">
                      <div class="info">Datum:</div> <br>
                      <div class="vrijednost">VAL</div> 
                  </div>
                  <div class="col-sm-1 col-sm-1  div_border cell">
                      <div class="info">Vrsta ziv:</div> <br>
                      <div class="vrijednost">VALUE</div> 
                  </div>
                  <div class="col-sm-3 div_border cell">
                      <div class="info">Broj zivotinja na kojima je provedena mjera:</div> <br>
                      <div class="vrijednost">VAL</div> 
                  </div>
                  <div class="col-sm-4 div_border cell">
                      <div class="info">Naziv i oznaka vakcine(serija):</div> <br>
                      <div class="vrijednost">VAL</div> 
                  </div>
              </div>
              
              
          </div>


          <div class="row">
                        
              <div class="col-sm-12">
                  <div class="col-sm-2 div_border cell">
                      <div class="info">Izvrsena vakcinacija (bolest):</div> <br>
                      <div class="vrijednost">ANTRAX</div> 
                      </div>
                  <div class="col-sm-1 div_border cell">
                      <div class="info">Datum:</div> <br>
                      <div class="vrijednost">VAL</div> 
                  </div>
                  <div class="col-sm-1 div_border cell">
                      <div class="info">Datum:</div> <br>
                      <div class="vrijednost">VAL</div> 
                  </div>
                  <div class="col-sm-1 div_border cell">
                      <div class="info">Vrsta ziv:</div> <br>
                      <div class="vrijednost">VALUE</div> 
                  </div>
                  <div class="col-sm-3 div_border cell">
                      <div class="info">Broj zivotinja na kojima je provedena mjera:</div> <br>
                      <div class="vrijednost">VAL</div> 
                  </div>
                  <div class="col-sm-4 div_border cell">
                      <div class="info">Naziv i oznaka vakcine(serija):</div> <br>
                      <div class="vrijednost">VAL</div> 
                  </div>
              </div>

          </div>
          

        </div>




	 	</div>
	


    

<!--     <div class="page">
    {{-- <div class="subpage">Page 2/2</div>     --}}
</div> -->
</div>

<script src="{{ url('metronic') }}/assets/global/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
<script src="{{ url('metronic') }}/assets/global/plugins/bootstrap-switch/js/bootstrap-switch.min.js" type="text/javascript"></script>