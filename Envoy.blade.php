@servers(['web' => 'd_o'])

<?php
$repo = 'git@bitbucket.org:amarh/fmpvs_veterinari.git';
$release_dir = '/home/veterinari/web/veterinari.hajrovica.com/_releases';
$app_dir = '/home/veterinari/web/veterinari.hajrovica.com/public_html';
$release = 'release_' . date('Y_m_d_Hi');
?>

@macro('deploy', ['on' => 'web'])
    fetch_repo
    run_composer
    update_permissions
    update_symlinks
@endmacro

@task('fetch_repo')
    [ -d {{ $release_dir }} ] || mkdir {{ $release_dir }};
    cd {{ $release_dir }};
    git clone {{ $repo }} {{ $release }};
@endtask

@task('run_composer')
    cd {{ $release_dir }}/{{ $release }};
    composer install --prefer-dist;
    cd {{ $app_dir }};
    cd ..;
    cp .env {{ $release_dir }}/{{ $release }};
    cd {{ $release_dir }}/{{ $release }};
    {{-- php artisan migrate:reset; --}}
    {{-- php artisan migrate --step --seed; --}}
@endtask

@task('update_permissions')
    cd {{ $release_dir }};
    chgrp -R www-data {{ $release }};
    chmod -R ug+rwx {{ $release }};
@endtask

@task('update_symlinks')
    ln -nfs {{ $release_dir }}/{{ $release }} {{ $app_dir }};
    chgrp -h www-data {{ $app_dir }};
@endtask