<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // $this->call(UserTableSeeder::class);
        // $this->call(QuestionsTableSeeder::class);
        // 

        
        DB::table('stanice')->insert([
                    'stanica'   => "Stanica 001",
                    'vlasnistvo'=> 'javno',
                    'kanton'    => 'KS',
                    'mjesto'    =>  'Sarajevo',
                    'pbroj'     =>  '71000',
                    'adresa'    =>  'Adresa', 
                    'telefon'   =>  '033 000 000',
                    // 'email'     =>  'test@ema'
                    'status'    =>  1
                ]);

        DB::table('users')->insert(
                [
                    'name' => "AmarH",
                    'email' => 'hajrovica@gmail.com',
                    'password' => bcrypt('110477'),
                    'level' =>  1,
                    'stanica_id' =>  1
                ]);
            
        DB::table('users')->insert(
                [
                    'name' => "Test User",
                    'email' => 'test@test.com',
                    'password' => bcrypt('110477'),
                    'level' =>  2,
                    'stanica_id' =>  1
                ]);
            }
}
