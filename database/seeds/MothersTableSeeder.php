<?php

use Illuminate\Database\Seeder;

// composer require laracasts/testdummy
use Laracasts\TestDummy\Factory as TestDummy;

class MothersTableSeeder extends Seeder
{
    public function run()
    {
        TestDummy::times(33)->create('App\Mother');
    }
}
