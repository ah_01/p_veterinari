<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateImanjaTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('imanja', function (Blueprint $table) {
            $table->increments('id'); 

            $table->string('id_broj');
            $table->string('pg_broj');
            
            $table->string('ime');
            $table->string('jmbg');
            
            $table->string('adresa');
            $table->string('mjesto');
            $table->string('pbroj');
            $table->string('kanton');
            
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('imanja');
    }
}
