<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePreglediTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
         Schema::create('pregledi', function (Blueprint $table) {
            $table->increments('id');
            $table->string('stanica_id');

            $table->string('imanje_id_broj');
            $table->string('imanje_ime');
            $table->string('imanje_adresa');
            $table->string('imanje_mjesto');
            $table->string('imanje_pbroj');

            //govedo id
            // $table->integer('govedo_id');

            //next block will be oboslete with govedo ID - goveda table
            $table->string('broj_zivotinje');
            $table->string('spol_zivotinje');
            $table->string('imanje_vrsta_zivotinje');
            $table->integer('imanje_ukupan_broj_zivotinja')->unsigned();

            $table->date('datum_pregled');
            $table->string('tip_pregled');
            $table->string('naziv_pregled');
            $table->string('rezultat_pregled');

            $table->string('broj_nalaza');
            $table->date('datum_nalaza')->nullable()->default(null);
            $table->string('naziv_oznaka_vakcine');


            $table->text('komentar');

            $table->integer('imanje_id')->unsigned()->nullable();
            $table->integer('zivotinja_id')->unsigned()->nullable();
            //user id
            $table->integer('user_id');
            //rcrd status unused
            $table->boolean('rcrd_stat');



            // $table->timestamps();
            $table->nullableTimestamps();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('pregledi');
    }
}
