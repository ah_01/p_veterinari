<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateStaniceTable extends Migration
{
 public function up()
    {
        Schema::create('stanice', function (Blueprint $table) {
            $table->increments('id');
            $table->string('stanica');
            $table->text('vlasnistvo');
            $table->text('kanton');
            $table->text('mjesto');
            $table->integer('pbroj');
            $table->text('adresa');
            $table->text('komentar');
            $table->text('telefon');
            $table->string('email')->nullable();
            $table->boolean('status');
            // $table->timestamps();
            $table->nullableTimestamps();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('stanice');
    }
}
