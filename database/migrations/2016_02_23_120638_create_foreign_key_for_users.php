<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateForeignKeyForUsers extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //


        Schema::table('users', function ($table) {
            $table->integer('stanica_id')->unsigned()->nullable();
            // $table->foreign('stanica_id')->references('id')->on('stanice');
        });




    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::table('users', function ($table) {
            // $table->dropForeign('users_stanica_id_foreign');
            $table->dropColumn('stanica_id');
        });
    }
}
