<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateGovedaTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
       
        if (!Schema::hasTable('zivotinje')) {
            Schema::create('zivotinje', function (Blueprint $table) {
                       $table->increments('id');

                       //next block will be obosete with govedo ID
                       $table->string('broj_zivotinje');
                       $table->string('spol_zivotinje');
                       $table->string('vrsta_zivotinje');
                       $table->string('status_zivotinje')->default('aktivna');

                       $table->integer('imanje_id')->unsigned();

                       
                       // This number will counted from actual animals on imanje 
                       $table->integer('ukupan_broj_zivotinja')->unsigned();


                       $table->timestamps();
                   });

        }

        Schema::table('zivotinje', function ($table) {
           
            $table->foreign('imanje_id')->references('id')->on('imanja');        
          
           
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('zivotinje', function ($table) {
            $table->dropForeign('zivotinje_imanje_id_foreign');        
            // $table->dropColumn('stanica_id');        
        });
        Schema::drop('zivotinje');
    }
}
