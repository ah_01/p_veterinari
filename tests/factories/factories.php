<?php

$factory('App\Question', [
    'title' => $faker->sentence(3),
    'question' => $faker->text(199),
    'status' => $faker->numberBetween(0, 0),
    'media' => $faker->randomElement(['email', "facebook"]),
    'created_at' => $faker->dateTimeBetween($startDate = '-11 days', $endDate = 'now'),
    'updated_at' => $faker->dateTimeBetween($startDate = '-7 days', $endDate = 'now'),

    
]);

$factory('App\Mother', [
	'datum'	=>  $faker->dateTimeBetween($startDate = '-11 days', $endDate = 'now'),
	'ime'	=>	$faker->firstNameFemale ,
	'pol' => 	$faker->randomElement(['M', "Z"]),
	'grad'	=>	$faker->city,
	'adresa'	=>	$faker->address,
	'telefon'	=>	$faker->phoneNumber, 
	'datum'	=>	$faker->dateTimeBetween($startDate = '-3 days', $endDate = 'now'), 
    
]);
$factory('App\Stanica', [
            'stanica'   => $faker->name(3),
            'vlasnistvo'=> $faker->randomElement(['privatno', "javno"]),
            'kanton'    => $faker->state(),
            'mjesto'    =>  $faker->city,
            'pbroj'     =>  $faker->postcode,
            'adresa'    =>  $faker->address,
            'komentar'  => $faker->text(99),
            'telefon'   => $faker->phoneNumber(),
            'email'     => $faker->email(),
            'status'    => $faker->randomElement([0, 1]),
    
]);
$factory('App\Pregled', [
            'imanje_id_broj'   => $faker->randomNumber($nbDigits = 6, $strict = true),
            'stanica_id'   => $faker->numberBetween(1,7),
            'broj_zivotinje'   => $faker->randomNumber($nbDigits = 6, $strict = true),
            'datum_pregled'   => $faker->dateTimeBetween($startDate = '-11 days', $endDate = '-7 days'),
            'tip_pregled'   => $faker->randomElement(['pregled', "vakcina"]),
	    	'naziv_pregled'	=> $faker->name(3),
            'rezultat_pregled'=> $faker->randomElement(['poz(+)', "neg(-)"]),
            'broj_nalaza'	=> $faker->randomNumber($nbDigits = 6, $strict = true),
            'datum_nalaza'	=>	$faker->dateTimeBetween($startDate = '-4 days', $endDate = 'now'),
            'naziv_oznaka_vakcine'		=>	$faker->text(11),
            'komentar'	     =>	$faker->words(21),
    
]);