<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Imanje extends Model
{
    protected $table = 'imanja';
    //
    //
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [

            'id_broj',
            'pg_broj',
            'ime',
            'jmbg',
            'adresa',
            'mjesto',
            'pbroj',



        ];

    public function pretraga($termini)
    {
        $query  = $this::where('id', '>', 0);
        foreach ($termini as $key => $value) {
            if (in_array($key, $this->fillable) && !empty($value)) {
                $query->where("$key", 'LIKE', '%' . $value . '%');
            }
        }
        return $query->paginate(50);
    }


    /**
     * hasMany relationship Imanje -> hasMany ->zivotinje
     * @return [type] [description]
     */
    public function zivotinje()
    {
        return $this->hasMany('App\Zivotinja');
    }
    /**
     * hasMany relationship for pregledi
     */
    public function pregledi()
    {
        return $this->hasMany('App\Pregled');
    }

    public function preglediGroupDateBy($val)
    {
        return $this->pregledi->groupBy(function ($item) use ($val) {
            return $item->datum_pregled->$val;
            //  return $item->datum_pregled->toFormattedDateString();
        });
    }
}
