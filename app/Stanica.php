<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Stanica extends Model
{
    //
    // protected $dates = ['datum'];
    // public $timestamps = false;
    protected $table = "stanice";

    protected $fillable = [
            'stanica',
            'vlasnistvo',
            'kanton',
            'mjesto',
            'pbroj',
            'adresa',
            'komentar',
            'telefon',
            'email',
            'status',
    ];


    public function users()
    {
        return $this->hasMany('App\User');
    }

    public function imanja()
    {
        return Imanje::where('mjesto', $this->mjesto);
    }
}
