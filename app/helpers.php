<?php
function show_dump($val)
{
    if (config('app.debug') == true) {
        dump($val);
    }
}

function getuser()
{
    return Auth::user();
}


/**
 * Sanitazie specific input for array value
 */
function sanitazeStringFromArr($arr, $name)
{
    $input = $arr;

    $input[$name] = preg_replace('/\s+/', '', $input[$name]);

    return $input;
}
