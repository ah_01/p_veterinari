<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Zivotinja;

class Zivotinja extends Model
{
	    protected $table = 'zivotinje';
	    protected $fillable = [
	    	'broj_zivotinje',
	    	'spol_zivotinje',
	    	'vrsta_zivotinje',
	    	'status_zivotinje',

	    	'imanje_id',
	    	'ukupan_broj_zivotinja',
	    ];
    //
    //


		public function getZivotinja($zivotinja_broj)
		{
			return Zivotinja::where('broj_zivotinje', $zivotinja_broj)->get();
		}

    public function Imanje()
    {
        return $this->belongsTo('App\Imanje');

    }

    public function aktivneZivotinjeNum()
    {
        return $this->where('status_zivotinje', 'A')->get();
    }
}
