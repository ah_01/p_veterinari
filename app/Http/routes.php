<?php

/*
|--------------------------------------------------------------------------
| Routes File
|--------------------------------------------------------------------------
|
| Here is where you will register all of the routes in an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', function () {
    flash()->success("OLLLAAAAA!");
    return view('metronic.content');

/*
        flash()->success("OLLLAAAAA!");
        $p = \App\Pregled::all();
        dump($p);
        $js = $p->toJson();
        dump($js);
        dump(collect(json_decode($js))->each(function($d){
            dump($d);
        }));

 */

    // return view('welcome');
});

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| This route group applies the "web" middleware group to every route
| it contains. The "web" middleware group is defined in your HTTP
| kernel and includes session state, CSRF protection, and more.
|
*/

Route::group(['middleware' => ['web']], function () {
    Route::auth();
    Route::get('/', 'PregledController@index');
    Route::get('/home', 'PregledController@index');

    Route::get('stanice', 'StanicaController@index');
    Route::post('stanice/create', 'StanicaController@create');
    Route::get('stanice/import', 'StanicaController@import_xls');
    Route::post('stanice/import', 'StanicaController@import_xls_show');
    Route::get('stanice/{stanica}', 'StanicaController@edit');
    Route::put('stanice/{stanica}/edit', 'StanicaController@update');
    Route::get('stanice/{stanica}/delete', 'StanicaController@destroy');

    Route::get('imanja/{imanje}/d', 'ImanjeController@destroy');
    Route::post('imanja/pretraga', 'ImanjeController@pretraga');
    Route::get('imanja/import', 'ImanjeController@import_xls');
    Route::post('imanja/import', 'ImanjeController@import_xls_show');
    Route::get('imanja/{id?}/{gid?}', 'ImanjeController@index');
    Route::post('imanja/create', 'ImanjeController@create');
    Route::put('imanja/{imanje}/edit', 'ImanjeController@update');

    Route::get('zivotinja/import', 'ZivotinjaController@import_xls');
    Route::post('zivotinja/import', 'ZivotinjaController@import_xls_show');
    Route::get('zivotinja/{id?}', 'ZivotinjaController@index');
    Route::post('zivotinja/create', 'ZivotinjaController@create');
    Route::put('zivotinja/{zivotinja}/edit', 'ZivotinjaController@update');
    Route::get('zivotinja/{zivotinja}/d', 'ZivotinjaController@destroy');


    Route::get('korisnici', 'UserController@index');
    Route::post('korisnici/create', 'UserController@create');
    Route::get('korisnici/{id}/delete', 'UserController@destroy');
    Route::get('korisnici/{korisnik}', 'UserController@show');
    Route::put('korisnici/{korisnik}/edit', 'UserController@update');

    Route::get('pregledi', 'PregledController@index');
    Route::get('pregledi/create/{imanje}', 'PregledController@edit');
    Route::post('pregledi/create', 'PregledController@create');
    Route::get('pregledi/import', 'PregledController@import_xls');
    Route::post('pregledi/import', 'PregledController@import_xls_show');

    Route::get('pregledi/print', 'PregledController@printp');
    Route::post('pregledi/print', 'PregledController@printp');
    Route::post('pregledi/printp', 'PregledController@testPrint');

    Route::get('pregledi/export', 'PregledController@export_xls');
    Route::get('pregledi/{pregled}', 'PregledController@show');
    Route::put('pregledi/{pregled}/edit', 'PregledController@update');
    Route::get('pregledi/p/{id}/{godina?}', 'PregledController@index');
    Route::post('pregledi/pretraga', 'PregledController@pretraga');
    Route::get('pregledi/godina/{godina}/{imanje?}', 'PregledController@pretragaGodina');
    // Route::get('pregledi/pretraga/{godina}', 'PregledController@pretraga');




    Route::get('pitanja', 'QuestionController@index');
    Route::get('pitanja/create', 'QuestionController@create');
    Route::post('pitanja/create', 'QuestionController@update');
    Route::get('pitanja/{pitanje}', 'QuestionController@edit');
    Route::put('pitanja/{pitanje}/edit', 'QuestionController@update');

    Route::get('tasks', 'TaskController@index');
    Route::post('tasks', 'TaskController@store');
    Route::delete('tasks/{id}', 'TaskController@delete');
    Route::put('tasks/{id}', 'TaskController@close');

    // Api Json routes
//
    Route::get('api/zivotinje', function(){
        $input = Input::get('option');

        $zivotinje = \App\Zivotinja::where('imanje_id', $input)->where('status_zivotinje', 'A')->lists('broj_zivotinje', 'id');
        // $zivotinje = \App\Zivotinja::where(function ($query, $input=$input) {
        //         $query->where('imanje_id', $input)
        //               ->where('status_zivotinje', 'A');
        //     })->lists('broj_zivotinje', 'id');
        // dd($zivotinje);

        return $zivotinje;
    });

    Route::get('api/imanje', function(){
        $input = Input::get('option');
        // dd($input);
        // $imanje = \App\Pregled::where('imanje_id_broj',  $input)->distinct()
        //         ->whereNotIn('imanje_ime',[''])->lists('imanje_ime');

        $imanje = \App\Imanje::where('id', $input)->get();
        // dd($imanje);
        return $imanje->toArray();
    });
    Route::get('api/kanton', function(){
        $input = Input::get('option');
        // dd($input);
        // $imanje = \App\Pregled::where('imanje_id_broj',  $input)->distinct()
        //         ->whereNotIn('imanje_ime',[''])->lists('imanje_ime');

        $imanje = \App\Imanje::where('kanton', $input)->get();
        // dd($imanje);
        return $imanje->toArray();
    });
});




    Route::get('mame', 'MotherController@index');
    Route::post('mame/upload', 'MotherController@upload');
    Route::get('mame/exp', 'MotherController@exportXls');
    Route::get('mame/test', 'MotherController@testMama');
    Route::get('mame/pdf', 'MotherController@pdf');
    Route::get('mame/close', 'MotherController@close');

    Route::get('inventar', 'InventarController@index');
    Route::post('inventar', 'InventarController@show');


// Route::group(['middleware' => 'web'], function () {
//     Route::auth();

//     Route::get('/home', 'HomeController@index');
// });
