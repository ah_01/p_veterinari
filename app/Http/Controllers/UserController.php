<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Http\Requests;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Gate;
use Illuminate\Support\Facades\Hash;

class UserController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if (Gate::denies('exit')) {
            // abort(503);
            return response()->view('errors.403', ['message', "Unauthorized!"], 403);
        }
        $users = User::all();

        return view('metronic.pages.korisnici')->with('data', $users);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        //
        if (Gate::denies('exit')) {
            // abort(503);
            return response()->view('errors.403', ['message', "Unauthorized!"], 403);
        }
        //
        //
        $test = $this->validate($request, [
               'name' => 'required|max:255',
               'email' => 'required|email|unique:users',
               // 'phone' => 'required|max:14',
               'password' => 'required|max:14',
           ]);


        $data = $request->all();
        $data['stanica_id'] = 1;
        $data['password'] = Hash::make($request->password);

        $u = User::create($data);

        return redirect('/korisnici');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(User $korisnik)
    {
        //
        //
        if (getuser()->cannot('exit')) {
            // abort(503);
            return response()->view('errors.403', ['message', "Unauthorized!"], 403);
        }

        $korisnik['stanice'] = \App\Stanica::all()->lists('stanica', 'id');
        return view('metronic.parts.korisnik')->with('korisnik', $korisnik);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, User $korisnik)
    {
        $this->validate($request, [

        'name' => 'required|max:255',
        'email' => 'required|email',
        'phone' => 'required|max:14',
        // 'password' => 'required|max:14',
             // 'status'    => 'required',
         ]);
        $data = $request->all();
        $data['password'] = Hash::make($request->password);
        if ($request->password == "") {
            $data['password'] = $korisnik->password;
        }


        $korisnik->fill($data);

        $korisnik->save();
        flash()->success("Korisnik $korisnik->id | $korisnik->name izmjenjen!");
        return redirect('/korisnici');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        User::destroy($id);
        return back();
    }
}
