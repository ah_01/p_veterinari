<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Http\Requests;
use App\Stanica;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Gate;
use League\Csv\Reader;

class StanicaController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $q = Stanica::all();

        return view('metronic.pages.stanice')->with('stanice', $q)->with('m', array('due'=>'1100'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        // dump($request);
        if (Gate::denies('exit')) {
            // abort(503);
            return response()->view('errors.403', ['message', "Unauthorized!"], 403);
        }
        // dump($stanica);
        $this->validate($request, [

         'stanica'   => 'required',
         'vlasnistvo' => 'required',
         'kanton'    => 'required',
         'mjesto'    => 'required',
         'pbroj'   => 'required',
         'adresa'    => 'required',
         // 'komentar'    => 'required',
         'telefon'   => 'required',
         // 'email'   => 'required',
         // 'status'    => 'required',
     ]);

        $s = Stanica::create($request->all());

        if ($s->id != null) {
            flash()->success("Stanica $s->id | $s->stanica kreirana!");
            return redirect('/stanice');
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Stanica $stanica)
    {
        dump($stanica);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Stanica $stanica)
    {
        if (Gate::denies('view-stanica', $stanica)) {
            // abort(503);
            return response()->view('errors.403', ['message', "Unauthorized!"], 403);
        }

        return view('metronic.parts.stanica')->with('stanica', $stanica);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Stanica $stanica)
    {
        $this->validate($request, [

            'stanica'   => 'required',
            'vlasnistvo' => 'required',
            'kanton'    => 'required',
            'mjesto'    => 'required',
            'pbroj'   => 'required',
            'adresa'    => 'required',
            // 'komentar'    => 'required',
            'telefon'   => 'required',
            // 'email'   => 'required',
            // 'status'    => 'required',
        ]);


        $stanica->fill($request->all());

        // dd($pitanje);
        $stanica->save();
        flash()->success("Stanica $stanica->id | $stanica->stanica izmjenjena!");
        return redirect('/stanice');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if (Gate::denies('exit')) {
            // abort(503);
            return response()->view('errors.403', ['message', "Unauthorized!"], 403);
        }
        Stanica::destroy($id);
        flash()->success("Teeeest");
        return redirect('/stanice');
    }


    /**
     * Show textarea to paste excel
     *
     * @param
     * @return
     */
    public function import_xls()
    {
        if (Gate::denies('exit')) {
            // abort(503);
            return response()->view('errors.403', ['message', "Unauthorized!"], 403);
        }
        return view('metronic.pages.txt_xls')->with('link', '/stanice/import');
    }

    /**
     * Show textarea to paste excel and import to pregledi
     * generate export from DB as template file?
     *
     * @param
     * @return
     */
    public function import_xls_show(Request $r)
    {
        $reader = Reader::createFromString($r->input('txt'));

        $reader->setDelimiter("\t");

        $reader->output();


        $results = $reader->fetchAll();


        $keys = $reader->fetchOne();
        $results = $reader->fetchAssoc(0);

        foreach ($results as $row) {
            //do something here
            $stanica = Stanica::create($row);
            $stanica->save();
        }
        return ('/stanice');
    }
}
