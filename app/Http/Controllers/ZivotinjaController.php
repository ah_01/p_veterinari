<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Http\Requests;
use App\Imanje;
use App\Zivotinja;
use Illuminate\Http\Request;
use League\Csv\Reader;
use Validator;

class ZivotinjaController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');

        $this->required_fields = [
            'broj_zivotinje' => 'required|unique:zivotinje|size:14',
            'spol_zivotinje'  => 'required|size:1',
            'vrsta_zivotinje'  => 'required',
            'status_zivotinje'  => 'required',

            'imanje_id'  => 'required',
            // 'ukupan_broj_zivotinja'  => '',
            ];
    }
    //
    //
    public function index(Request $r)
    {
        $data['data'] = Zivotinja::orderBy('id', 'desc')->take(1000)->get();
        $data['data'] = Zivotinja::paginate(1000);
        $data['zivotinja'] = Zivotinja::find($r->segment(2));
        $data['imanja'] = Imanje::distinct()->lists("id_broj", "id");

        // $data['test'] = "TEEEEST";
        return view('metronic.zivotinja.lista', $data);
    }

    public function create(Request $r)
    {
        // dd($r->all());
        $this->validate($r, $this->required_fields);
        //
        Zivotinja::create($r->all());
        flash()->success("Zivotinja kreirana!");

        // return redirect('/zivotinja');
        return back();
    }

    public function update(Request $r, Zivotinja $zivotinja)
    {
        // dd($zivotinja);
        // $this->validate($r, $this->required_fields);
        $zivotinja->fill($r->all());
        $zivotinja->save();

        // return redirect('/zivotinja');
        return redirect('/imanja/'.$zivotinja->imanje_id);
    }

    public function destroy(Zivotinja $zivotinja)
    {
        $zivotinja->delete();
        return redirect('/zivotinja');
    }

    /**
     * Sanitazie specific input for array value
     */
    public function sanitaze($r)
    {
        $input = $r;

        $input['broj_zivotinje'] = preg_replace('/\s+/', '', $input['broj_zivotinje']);
        // dd($input);

        return $input;
    }


    /**
     * Desc: sanitize row data, check if animal does not exist in table,if exists exit with false
     * if not get imanje by id_broj in table, add imanje id to row data (it is eequired by Zivotinja model)
     * add zivotinja and return true
     *
     * @param array $row input data for Zivotinja model
     * @return bool true false
     * @funct add new Zivotinja maintaining link to related table imanja
     *
     */
    public function ZivotinjaInsert($row)
    {
        // dd($this->rules($row));
        // dd($row);
        // $row = $this->sanitaze($row);

        $row = sanitazeStringFromArr($row, 'broj_zivotinje');
        $row = sanitazeStringFromArr($row, 'id_broj');

        $validator = Validator::make($row, [

        'broj_zivotinje' => 'required|unique:zivotinje',
        'id_broj'  => 'required',
      ]);

        if ($validator->fails()) {
            return false;
        }

        $imanje = Imanje::where('id_broj', $row['id_broj'])->get();
        $row['imanje_id'] = $imanje->first()->id;
        // dd($row);

        $zivotinja = Zivotinja::create($row);
        $zivotinja->save();
        flash()->success("Zivotinja kreirana!");
        return true;
    }


    // ##########################################################################################################
//

    /**
     * Show textarea to paste excel
     *
     * @param
     * @return view
     */
    public function import_xls()
    {
        return view('metronic.pages.txt_xls')->with('link', '/zivotinja/import');
    }

    /**
     * Show textarea to paste excel and import to pregledi
     * generate export from DB as template file?
     *
     * @param
     * @return
     */
    public function import_xls_show(Request $r)
    {
        // dd($r->input('txt'));

        $reader = Reader::createFromString($r->input('txt'));


        // foreach ($reader as $index => $row) {
        //     dump ($row);
        //     dump ($index);

        // }
        //
        $reader->setDelimiter("\t");
        // $reader->setEscape('\\');

        // $reader->output();

        // $func = function ($row) {
        //     return array_map('strtoupper', $row);
        // };


        // $results = $reader->fetchAll();
        $results = $reader->fetchAll();

        /**
         * make collection niz from csv  results
         */
        $niz = collect(($results));

        /**
         * get first row as head
         */
        $head  = collect($niz->shift());

        /**
         * Make new arr to combine 2 exsisting
         */
        $combined = collect();

        /**
         * Combine niz and head to get propper assoc array
         */
        foreach ($niz as $key) {
            $combined[]=($head->combine($key)->toArray());
        }

        // dump($combined);
        // dd($niz);
        // $keys = ['Šifra gazdinstva', 'Broj ušne markice', 'Opæina', 'Adresa', 'Naziv vet. organizacije', 'Datum apl. tuber.', 'Datum kontrole', 'Rezultat', 'Komentar'];
        // $keys = $reader->fetchOne();
        // $results = $reader->fetchAssoc(0);

        /**
         * fo through each row and add call ZivotinjaInsert function while outputing value
         */
        foreach ($combined as $row) {
            dump($this->ZivotinjaInsert($row));
        }
        // $xls = \Excel::sheet($r->input('txt'))->get();

        // dd($xls);

        /**
         * Return back to import form
         */
        // return back();
    }

    //
    // ##########################################################################################################
}
