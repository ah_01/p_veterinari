<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Http\Requests;
use App\Imanje;
use App\Pregled;
use App\Zivotinja;
use Carbon\Carbon;
use Illuminate\Http\Request;
use League\Csv\Reader;
use League\Csv\Writer;
use Validator;

class PregledController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
        $this->required_fields =
           [

            'stanica_id' => 'required',
            'imanje_id_broj'  => 'required',
            'imanje_id'  => 'required',
            'imanje_ime'  => 'required',
            'imanje_adresa' => 'required',
            'imanje_mjesto' => 'required',
            'imanje_pbroj' => '',
            'imanje_vrsta_zivotinje' => '',
            'imanje_ukupan_broj_zivotinja' => '',

            'broj_zivotinje' => 'required',
            'datum_pregled' => 'required',
            'tip_pregled' => 'required',
            'naziv_pregled' => 'required',
            'rezultat_pregled',
            'broj_nalaza',
            // 'datum_nalaza',
            'naziv_oznaka_vakcine',
            'komentar',
            'user_id' => 'required',
            'rcrd_stat',

             ];
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($id=null)
    {
        if ($id == "stanica") {
            $pregledi = Pregled::where('stanica_id', getuser()->stanica_id)->get();
        }
        if ($id == "korisnik") {
            $pregledi = Pregled::where('user_id', getuser()->id)->get();
        }
        if ($id == "godina") {
            $pregledi = $this->pretraga(\Request::segment(4));
        }
        if (!$id) {
            $pregledi = Pregled::where('user_id', '=', getuser()->id)->get();
        }
        if (!$id && getuser()->level == 1) {
            $pregledi = Pregled::all();
        }

        if (getuser()->isSuperAdmin()) {
            $imanja = Pregled::lists("imanje_id_broj", "imanje_id_broj");
        } else {
            $imanja = Pregled::where('user_id', getuser()->id)->lists("imanje_id_broj", "imanje_id_broj");
        }

        $kantoni = Imanje::lists("kanton", "kanton");
        $kantoni->prepend(' --- ', ' --- ');



        return view('metronic.pages.pregledi')->with('data', $pregledi)->with('imanja', $imanja)->with('kantoni', $kantoni);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request, Zivotinja $z)
    {
        $input = $request->all();
        $zivotinje = $input['zivotinje'];

        $zivotinja_import = [];

        foreach ($zivotinje as $zivotinja_broj) {

            //Get Collection zivotinja by its number($zivotinja) from Zivotinja model and get only result as array
            $imanje = $z->getZivotinja($zivotinja_broj)->first()->imanje;
            $zivotinja = $z->getZivotinja($zivotinja_broj)->toArray()[0];

            //set key zivotinja_id on array for pregledi DB
            $zivotinja['zivotinja_id'] = $zivotinja['id'];
            $zivotinja['imanje_id_broj'] = $imanje->id_broj;
            $zivotinja['imanje_ime'] = $imanje->ime;
            $zivotinja['imanje_adresa'] = $imanje->adresa;
            $zivotinja['imanje_mjesto'] = $imanje->mjesto;
            $zivotinja['user_id'] = getuser()->id;
            $zivotinja['imanje_ukupan_broj_zivotinja'] = count($imanje->zivotinje->where('status_zivotinje', 'A'));
            $zivotinja['imanje_vrsta_zivotinje'] =   $zivotinja['vrsta_zivotinje'];

            //forget key id from zivotinja array - precaution only
            array_forget($zivotinja, 'id');

            //join zivotinja array with input
            $zivotinja_import = $input + $zivotinja;


            $validator = Validator::make($zivotinja_import, $this->required_fields);
            if ($validator->fails()) {
                dd($validator->errors());
                return back()
                     ->withErrors($validator)
                     ->withInput();
            }


            Pregled::create($zivotinja_import);
        }

        flash()->success("pregled kreiran!");
        return redirect('/pregledi');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Pregled $pregled)
    {
        $pregled['stanice'] = \App\Stanica::all()->lists('stanica', 'id');
        $pregled['pregled_naziv'] = Pregled::distinct()->lists("naziv_pregled");

        return view('metronic.parts.pregled')->with('pregled', $pregled);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Imanje $imanje)
    {
        // dump($imanje);
        return view('metronic.pages.create_pregled')->with('imanje', $imanje);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Pregled $pregled)
    {
        $reqFields = Pregled::all()->first()->req_fields;


        $this->validate($request, $reqFields);

        // dd($data);
        $pregled->fill($request->all());

        // dd($pitanje);
        $pregled->save();
        flash()->success("pregled $pregled->id | $pregled->id izmjenjen!");
        return redirect('/pregledi')->withInput();
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }


    /**
     * Show textarea to paste excel
     *
     * @param
     * @return
     */
    public function import_xls()
    {
        return view('metronic.pages.txt_xls')->with('link', '/pregledi/import');
    }
    /**
     * Show textarea to paste excel and import to pregledi
     * generate export from DB as template file?
     *
     * @param
     * @return
     */
    public function import_xls_show(Request $r)
    {
        // dump($r->input('txt'));

        $reader = Reader::createFromString($r->input('txt'));


        $reader->setDelimiter("\t");

        $reader->output();

        $results = $reader->fetchAll();

        $keys = $reader->fetchOne();
        $results = $reader->fetchAssoc(0);

        foreach ($results as $row) {
            //do something here

            //Change stanica id to users stanica_id - minimize errors
            $row['stanica_id'] = getuser()->stanica_id;
            $row['user_id'] = getuser()->id;
            // dump($row);
            $pregled = Pregled::create($row);
            $pregled->save();
        }
        // $xls = \Excel::sheet($r->input('txt'))->get();

        // dd($xls);
    }

    public function export_xls()
    {
        $pregled = Pregled::all();

        $csv = \League\Csv\Writer::createFromFileObject(new \SplTempFileObject());
        $csv->setDelimiter("\t");
        $csv->insertOne(\Schema::getColumnListing('pregledi'));

        $csv->output('pregledi.xls');
    }



    public function pretraga(Request $request, Pregled $pregled)
    {
        $godina = ($request->godina == "")? "2016" : $request->godina;
        $imanje = ($request->imanje_id_broj == "")? "0" : $request->imanje_id_broj;

        $imanja = Pregled::distinct()->lists("imanje_id_broj", "imanje_id_broj");

        $kantoni = Imanje::lists("kanton", "kanton");


        $pregledi = $pregled->pretraga1($request->all());

        return view('metronic.pages.pregledi')->with('data', $pregledi)
                                             ->with('imanja', $imanja)
                                             ->with('godina', $godina)
                                             ->with('imanje', $imanje)
                                             ->with('kantoni', $kantoni);
    }

    public function pretragaGodina($godina, Imanje $imanje, Request $request, Pregled $pregled)
    {
        if ($request->segment(3)) {
            $arr['godina'] = $request->segment(3);
        }

        if ($request->segment(4)) {
            $arr['imanje_id'] = $request->segment(4);
        }


        $pregledi = $pregled->pretraga1($arr);

        $imanja = Pregled::distinct()->lists("imanje_id_broj", "imanje_id_broj");

        return view('metronic.pages.pregledi')->with('data', $pregledi)
                                            ->with('imanja', $imanja)
                                            ->with('imanje', $imanje->id_broj)
                                            ->with('godina', $arr['godina']);
    }

    public function printp(Request $r)
    {
        $pregled = new Pregled();
        $pregledi = $pregled->pretraga1($r->all());

        return view('print.potvrda')->with('data', $pregledi);
    }

    public function testPrint(Request $r)
    {
        $pregled = new Pregled();
        $data = $pregled->pretraga($r->godina, $r->imanje);


        return \PDF::loadView('print.potvrda', ['data' => $data])->setPaper('a4')
        ->setOption('margin-bottom', 0)->setOption('margin-left', 0)->setOption('margin-top', 3)->setOption('margin-right', 0)->stream('github.pdf');
    }
}
