<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Task;
use App\Http\Requests;
use App\Http\Controllers\Controller;

class TaskController extends Controller
{
    //
	public function index()
	{
		$r = Task::where('status', 'open')->get();
		$a = Task::all();
		return view('metronic.pages.tasks')->with('open', $r)->with('all', $a);
	}

	public function store(Request $req)
	{
		 $this->validate($req, [
        	'name' => 'required|max:255',
   		 ]);

		Task::create($req->all());

		return redirect('/tasks');


	}

	public function delete($id)
	{
		Task::FindOrFail($id)->delete();
		return redirect('/tasks');
		
	}
	public function close($id)
	{
		$t = Task::FindOrFail($id);
		$t->status = "closed";
		$t->save();
		return redirect('/tasks');
	}
}
