<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Http\Requests;
use App\Imanje;
use App\Zivotinja;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Gate;
use League\Csv\Reader;

use Validator;

class ImanjeController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');

        $this->required_fields = [
            'id_broj'   =>'required|unique:imanja',
            // 'pg_broj'   =>'',
            'ime'   =>'',
            'jmbg'  =>'',
            'adresa'    =>'required',
            'mjesto'    =>'required',
            'pbroj' =>'required|numeric',
        ];
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $r, $id=null, $gid=null)
    {
        //


        /**
         * Case get imanje based on id url/imanja/imanjeid
         */
        $imanje = Imanje::where('id', $r->segment(2))->get();

        $imanja =  Imanje::distinct()->lists("id_broj", "id");


        /**
         * IF 3rd segment is "Sve" show all imanja wih pagination
         */
        if ($r->segment(3)=="sve") {
            $data = Imanje::orderBy('id', 'desc')->take(1000)->get();

            $data = Imanje::paginate(1000);
            return view('metronic.imanje.lista', compact(['zivotinja']))->with('data', $data)->with('imanja', $imanja);
        }

        /**
         * Basic functionality show related imanja for region  - mjesto in DB table
         */

        /**
         * Case show zivotinja on imanje url/imanja/idimanje/idzivotinja
         */
        if ($r->segment(3) && is_numeric($r->segment(3))) {
            $zivotinja = Zivotinja::find($r->segment(3));
        }


        /**
         * START: <AHA> Next block should be obsolete remove from fixed version </AHA>
         */
        if (count($imanje) == 1) {
            $data = $imanje;
        } else {
            $endDate = Carbon::now()->addHour();
            $startDate = Carbon::now()->subHour();


            $data = Imanje::where('updated_at', '>=', $startDate)->where('updated_at', '<=', $endDate)->paginate(500);
        }
        $data  = getuser()->stanica->imanja()->paginate(100);


        return view('metronic.imanje.lista', compact(['zivotinja']))->with('data', $data)->with('imanje', $imanje)->with('imanja', $imanja);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $r)
    {

        // Validate this
        $this->validate($r, $this->required_fields);

        //

        Imanje::create($r->all());
        flash()->success("Imanje kreirano!");

        return redirect('/imanja');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */

    public function store($r)
    {
        $r = sanitazeStringFromArr($r, 'id_broj');
        $validator = Validator::make($r, [
          'id_broj'   =>'required|unique:imanja',
          // 'pg_broj'   =>'',
          'ime'   =>'',
          'jmbg'  =>'',
          'adresa'    =>'required',
          'mjesto'    =>'required',
          'pbroj' =>'',
      ]);

        if ($validator->fails()) {
            return false;
        }

        Imanje::create($r);
        flash()->success("Imanje kreirano!");

        return true;
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Imanje $imanje)
    {
        // Validate::Input

        $imanje->fill($request->all());
        $imanje->save();

        return redirect('/imanja');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Imanje $imanje)
    {
        if (getuser()->cannot('exit')) {
            // abort(503);
            return response()->view('errors.403', ['message', "Unauthorized!"], 403);
        }

        $imanje->delete();
        return redirect('/imanja');
    }


    /**
     * Show textarea to paste excel
     *
     * @param
     * @return
     */
    public function import_xls()
    {
        if (Gate::denies('exit')) {
            // abort(503);
            return response()->view('errors.403', ['message', "Unauthorized!"], 403);
        }
        return view('metronic.pages.txt_xls')->with('link', '/imanja/import');
    }

    /**
     * EXCEL import to imanja
     * generate export from DB as template file?
     *
     * @param
     * @return
     */
    public function import_xls_show(Request $r)
    {
        $reader = Reader::createFromString($r->input('txt'));


        $reader->setDelimiter("\t");


        $results = $reader->fetchAll();

        $keys = $reader->fetchOne();
        $results = $reader->fetchAssoc(0);

        $counter = 0;
        foreach ($results as $row) {
            $counter++;
            if ($counter%100 == 0) {
                echo("SLEEEEEEEEEP <br> <br> <br>");
                sleep(1);
            }

            dump($this->store($row));
        }
        return back();
    }


    public function pretraga(Request $r, Imanje $imanje)
    {
        $data = $imanje->pretraga($r->all());
        return view('metronic.imanje.lista')->with('data', $data);


        // dd($imanja);
    }
}
