<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class Pregled extends Model
{
    protected $table = 'pregledi';

    protected $dates = ['created_at', 'updated_at', 'datum_pregled'];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [

    'stanica_id',
    'imanje_id_broj',
    'imanje_ime',
    'imanje_adresa',
    'imanje_mjesto',
    'imanje_pbroj',

    'broj_zivotinje',
    'spol_zivotinje',
    'imanje_vrsta_zivotinje',
    'imanje_ukupan_broj_zivotinja',
    'zivotinja_id',


    'datum_pregled',
    'tip_pregled',
    'naziv_pregled',
    'rezultat_pregled',
    'broj_nalaza',
    'datum_nalaza',
    'naziv_oznaka_vakcine',
    'komentar',
    'user_id',
    'rcrd_stat',
    'imanje_id',


    ];

    protected $requiredFields = [

        'stanica_id' => 'required',
        'imanje_id_broj'  => 'required',
        'imanje_ime'  => 'required',
        'imanje_adresa' => 'required',
        'imanje_mjesto' => 'required',
        'imanje_pbroj' => 'required',
        'imanje_vrsta_zivotinje' => '',
        'imanje_ukupan_broj_zivotinja' => '',

        'broj_zivotinje' => 'required',
        'datum_pregled' => 'required',
        'tip_pregled' => 'required',
        'naziv_pregled' => 'required',
        'rezultat_pregled',
        'broj_nalaza',
        // 'datum_nalaza',
        'naziv_oznaka_vakcine',
        'komentar',
        'user_id' => 'required',
        'rcrd_stat',

         ];

    public function pretraga($godina = "2016", $imanje="")
    {
        $startDate = Carbon::create($godina, 1, 1);
        $endDate = Carbon::create($godina+1, 1, 1);
        $pregled = Pregled::where('datum_pregled', '>=', $startDate)->where('datum_pregled', '<=', $endDate);
        if (($imanje != "")) {
            $pregled = $pregled->where('imanje_id_broj', $imanje);
        }

        return $pregled->get();
    }

    public function pretraga1($termini)
    {
        $godina = $termini['godina'];
        // dump($termini);
        if (!empty($godina)) {
            $startDate = Carbon::create($godina, 1, 1);
            $endDate = Carbon::create($godina+1, 1, 1);
            $query  = Pregled::where('datum_pregled', '>=', $startDate)->where('datum_pregled', '<=', $endDate);
        } else {
            $query  = $this::where('id', '>', 0);
        }

        /**
         * add search query based on arrays value passed  except godina above
         */
        foreach ($termini as $key => $value) {
            if (in_array($key, $this->fillable) && !empty($value)) {
                $query->where("$key", 'LIKE', '%' . $value . '%');
            }
        }
        return $query->get();
    }

    public function user()
    {
        return $this->belongsTo('App\User');
    }

    public function getReqFieldsAttribute()
    {
        return $this->requiredFields;
    }

    public function getDatumNalazaAttribute($value)
    {
        return Carbon::createFromDate();
    }
}
