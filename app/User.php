<?php

namespace App;

use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password', 'phone', 'stanica_id'
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];


    public function stanica()
    {
        return $this->belongsTo('App\Stanica');
    }

    public function pregledi()
    {
        return $this->hasMany('App\Pregled');
    }

    public function setStanicaIdAttribute($value)
    {
        $this->attributes['stanica_id'] = $value ?: null;
    }

    public function isSuperAdmin()
    {
        if ($this->level == 1) {
            return true;
        }
    }
}
